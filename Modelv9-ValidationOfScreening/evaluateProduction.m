function prodKPIs = evaluateProduction(started, completed,queueHist, operators, machines, releaseRate)
clear throughputTime;
clear operatorEfficiency;
prodKPIs = struct('throughput',0,'operatorEfficiency',0,'avgDelay',0);



for i=1:length(completed(:,1))
 %for i=1:length(completed)
    %actual time from production release to completion (including mounting and
    %unmount time)
    %throughputTime(i,1) = completed(i,3) - started(find(started(:,1)==completed(i,1)),3);
    throughputTime(i,1) = completed(i,3) - queueHist(find(queueHist(:,1)==completed(i,1)),2);
    
    %work content of completed job (including mounting and unmount
    %time)
    throughputTime(i,2) = completed(i,2);
    
 end

avgDelay = mean(throughputTime(:,1) - throughputTime(:,2));

%get throughput from slope of linear fit through all datapoints
pp=polyfit(single(completed(1:length(completed(:,1)),3)), single(cumsum(completed(1:length(completed),2))), 1);
throughput = pp(1);

%calculate operator utilization
countOperators = length(operators(:,1)) * 8 / 24;
totalWorktime = (max(completed(:,3)) - mod(max(completed(:,3)),24)) * countOperators + mod(max(completed(:,3)),24) * countOperators;

totalProductiveTime = 0.0;
for i=1:length(completed(:,1))
    totalProductiveTime = totalProductiveTime + machines(completed(i,4),1) + machines(completed(i,4),2);
end
operatorEfficiency = double(totalProductiveTime) / double(totalWorktime);

prodKPIs.throughput = throughput;
prodKPIs.operatorEfficiency = operatorEfficiency;
prodKPIs.avgDelay = avgDelay;
%prodKPIs.releaseRate = releaseRate;        why is this here????
end
