function schedules  = v3f02_createSched(jobCount, matCount, param1, param2)

% job / schedule parameters
% longestJob = str2num(longestJob);
%param1 --> distParam1 in the control window
%param2 --> distParam2 in the control window
%

%parametrize distribution
param3=1;
%distType = 'Beta';

%%%%clear schedule prodKPIs WIP;
distribution = struct('Type','Gamma','Param1',0,'Param2',0,'Param3',0);
stats = struct('avg',0,'std',0,'var',0);
paramCombinations = uint16(length(param1) * length(param2) * length(param3));
schedule=zeros(jobCount,4);
schedules = struct('schedule',schedule,'distribution',distribution,'stats',stats);

%dist=zeros(paramCombinations,3);
%stats=zeros(paramCombinations,3);
%schedules=zeros(jobCount,4,paramCombinations);

i = 1;
for eachParam1 = param1
    for eachParam2 = param2
        for eachParam3 = param3
            %%%%%i  Aziz commented this out
% %             distribution= createDist(eachParam1,eachParam2,eachParam3);
% %             
% %             schedules(:,:,i)=createSchedule(jobCount,matCount,distribution);
% %             
% %             dist(i,1)=eachParam1;
% %             dist(i,2)=eachParam2;
% %             dist(i,3)=eachParam3;
% %             
% %             stats(i,1)=mean(schedules(:,1,i));
% %             stats(i,2)=std(schedules(:,1,i));
% %             stats(i,3)=stats(i,2)/stats(i,1);
% %             
% %             i=i+1;


%%            
%%                   new version by Aziz
           % schedules = 


            dist = createDist(eachParam1,eachParam2,eachParam3);
            schedules(i).schedule = createSchedule(jobCount,matCount,dist);
           % schedules(i).distribution.Type = distType;
            schedules(i).distribution.Param1 = eachParam1;
            schedules(i).distribution.Param2 = eachParam2;
            schedules(i).distribution.Param3 = eachParam3;
            schedules(i).stats.avg = mean(schedules(1,i).schedule(:,1));
            schedules(i).stats.std = std(schedules(1,i).schedule(:,1));
            schedules(i).stats.var = schedules(i).stats.std / schedules(i).stats.avg;
            i = i+1;




            
            
% % %%            original code written by Tobi
% % %%                   replaced by Aziz
% %             dist = createDist(eachParam1,eachParam2,eachParam3);
% %             schedules(:,i).schedule = createSchedule(jobCount,matCount,dist);
% %             schedules(i).dist.Type = distType;
% %             schedules(i).dist.Param1 = eachParam1;
% %             schedules(i).dist.Param2 = eachParam2;
% %             schedules(i).dist.Param3 = eachParam3;
% %             schedules(i).stats.avg = mean(schedules(1,i).schedule(:,1));
% %             schedules(i).stats.std = std(schedules(1,i).schedule(:,1));
% %             schedules(i).stats.var = schedules(i).stats.std / schedules(i).stats.avg;
% %             i = i+1;
        end
    end
end


%Defines the schedule function used above
function schedule = createSchedule(jobCount,matCount,dist)

schedule = ones(jobCount, 4);

schedule(:,1) = ceil(random(dist,jobCount,1));
%randomly select a material
schedule(:,2) = randi(matCount,jobCount,1);

schedule(:,3) = schedule(:,1);
schedule(:,4) = zeros();

end

%Defines the function to create a distribution
function dist = createDist(param1,param2,param3)
%figure
%skewed normal distribution
% x=-5:0.1:5;
% normDist = @(x) (1/sqrt((2*pi))*exp(-x.^2/2));
% skewNormDist = @(x,alpha) 2*normDist(x).*normcdf(alpha*x);
%plot(x, skewNormDist(x, 6));

%f distribution
% x=0:0.1:5;
% plot(x, fpdf(x, 10,10));

%weibull distribution
%dist = makedist('Weibull',50,2.3);

%burr distribution
%dist = makedist('Burr',param1,param2,param3);

%gamma
%dist = makedist('Gamma',param1,param2);

%lognormal
%dist = makedist('Lognormal',param1,param2);

%uniform distribution
%dist = makedist('Uniform');

%normal distribution (gaussian)
%dist = makedist('Normal',param1,param2);

%weibull distribution
dist = makedist('Gamma',param1,param2);
end



end