%% create window for creating graphs, saving etc...

controlWin = uifigure('Position',[300 600 710 430]);
evalPanel = uipanel(controlWin,'Position',[300 10 400 250],'Title','Evaluation');
statsPanel = uipanel(evalPanel,'Position',[70 60 250 140],'Title','Stats');

schedPanel = uipanel(controlWin,'Position',[10 10 280 250],'Title','Schedule Creation');

prodPanel = uipanel(controlWin,'Position',[10 270 380 150],'Title','Production Settings');

simPanel =  uipanel(controlWin,'Position',[400 270 300 150],'Title','Simulation Settings');

%% ui controls in prodPanel
opCount24Text = uieditfield(prodPanel,'Position',[10 100 20 20], 'Value','0');
opCount24Label = uilabel(prodPanel,'Position',[40 100 100 20], 'Text','Ops @2400');

opCount08Text = uieditfield(prodPanel,'Position',[10 70 20 20], 'Value','1');
opCount08Label = uilabel(prodPanel,'Position',[40 70 100 20], 'Text','Ops @0800');

opCount16Text = uieditfield(prodPanel,'Position',[10 40 20 20], 'Value','1');
opCount16Label = uilabel(prodPanel,'Position',[40 40 100 20], 'Text','Ops @1600');

matAMachineCountText = uieditfield(prodPanel,'Position',[160 100 20 20], 'Value','1');
matAMachineCountLabel = uilabel(prodPanel,'Position',[190 100 100 20], 'Text','Machines Mat. A');
matBMachineCountText = uieditfield(prodPanel,'Position',[160 70 20 20], 'Value','0');
matBMachineCountLabel = uilabel(prodPanel,'Position',[190 70 100 20], 'Text','Machines Mat. B');
matCMachineCountText = uieditfield(prodPanel,'Position',[160 40 20 20], 'Value','0');
matCMachineCountLabel = uilabel(prodPanel,'Position',[190 40 100 20], 'Text','Machines Mat. C');

matAMachineMountText = uieditfield(prodPanel,'Position',[300 100 20 20], 'Value','1');
matBMachineMountText = uieditfield(prodPanel,'Position',[300 70 20 20], 'Value','1');
matCMachineMountText = uieditfield(prodPanel,'Position',[300 40 20 20], 'Value','1');
matAMachineUnmountText = uieditfield(prodPanel,'Position',[330 100 20 20], 'Value','1');
matBMachineUnmountText = uieditfield(prodPanel,'Position',[330 70 20 20], 'Value','1');
matCMachineUnmountText = uieditfield(prodPanel,'Position',[330 40 20 20], 'Value','1');

prodPanelInput = [opCount24Text, opCount08Text, opCount16Text, matAMachineCountText, matBMachineCountText, matCMachineCountText, matAMachineMountText, matBMachineMountText, matCMachineMountText, matAMachineUnmountText, matBMachineUnmountText, matCMachineUnmountText];

createProdButton = uibutton(prodPanel,'Position',[10 10 100 20],'Text','Create Prod. Env.','ButtonPushedFcn', @(createProdButton,event) createProdButtonPushed(createProdButton,prodPanelInput));

%% ui controls in evalPanel
param1Label = uilabel(statsPanel,'Position',[10 90 120 20], 'Text','Dist. P1: -');
param2Label = uilabel(statsPanel,'Position',[10 70 120 20], 'Text','Dist. P2: -');
avgLabel = uilabel(statsPanel,'Position',[10 50 120 20], 'Text','Avg. WC: -');
stdLabel = uilabel(statsPanel,'Position',[10 30 120 20], 'Text','Std. WC: -');
varLabel = uilabel(statsPanel,'Position',[10 10 120 20], 'Text','Var. WC: -');
TPLabel1 = uilabel(statsPanel,'Position',[130 90 120 20], 'Text','TP Avg: -');
OpLabel = uilabel(statsPanel,'Position',[130 50 120 20], 'Text','Op. Eff.: -');
delayLabel = uilabel(statsPanel,'Position',[130 30 110 20], 'Text','Avg. Delay: -');
TPLabel2 = uilabel(statsPanel,'Position',[130 70 120 20], 'Text','TP Med: -');
simCountLabel = uilabel(statsPanel,'Position',[190 10 40 20], 'Text','n: -');
countReSims = uieditfield(evalPanel,'Position',[10 30 30 20],'Value','10');

scheduleList = uilistbox(evalPanel,'Items',"-",'Position',[10 60 50 140], 'ValueChangedFcn', @(scheduleList, event) updateScheduleInfo(scheduleList,statsPanel,schedPanel));
% drawTPButton = uibutton(statsPanel,'Position',[130 10 110 20],'Text','Draw TP & hist','ButtonPushedFcn', @(drawTPButton,event) drawTPButtonPushed(drawTPButton,scheduleList,schedPanel,prodPanel));
simList = uidropdown(statsPanel,'Position',[130 10 50 20],'Items',"-",'ValueChangedFcn',@(selectedSim,event) drawTP(selectedSim,scheduleList,schedPanel,prodPanel),'Tag','simList');

reSimButton = uibutton(evalPanel,'Position',[50 30 120 20],'Text','Re-Sim Schedule','ButtonPushedFcn', @(reSimButton,event) reSimButtonPushed(reSimButton,scheduleList,schedPanel,prodPanel,simPanel,statsPanel,countReSims));
drawStatsButton = uibutton(evalPanel,'Position',[200 30 110 20],'Text','Draw Stats','ButtonPushedFcn', @(drawStatsButton,event) rangeFind(schedPanel.UserData));
saveButton = uibutton(evalPanel,'Position',[200 5 110 20],'Text','Save Data','ButtonPushedFcn', @(saveButton,event) saveButtonPushed(schedules,prodKPIs,WIP));

%% ui controls in schedPanel
% longestJobText = uieditfield(schedPanel,'Position',[10 170 50 20], 'Value','50');
% longestJobLabel = uilabel(schedPanel,'Position',[70 170 100 20], 'Text','Longest Job [h]');

jobCountText = uieditfield(schedPanel,'Position',[10 140 50 20], 'Value','1000');
jobCountLabel = uilabel(schedPanel,'Position',[70 140 100 20], 'Text','Job Count [#]');

matCountText = uieditfield(schedPanel,'Position',[10 110 50 20], 'Value','1');
matCountLabel = uilabel(schedPanel,'Position',[70 110 100 20], 'Text','Materials [#]');

distParam1Text = uieditfield(schedPanel,'Position',[10 80 50 20], 'Value','3:0.5:4');
distParam1Label = uilabel(schedPanel,'Position',[70 80 100 20], 'Text','Dist Param1');

distParam2Text = uieditfield(schedPanel,'Position',[10 50 50 20], 'Value','3');
distParam2Label = uilabel(schedPanel,'Position',[70 50 100 20], 'Text','Dist Param2');

createSchedButton = uibutton(schedPanel,'Position',[10 20 100 20],'Text','Create Schedule','ButtonPushedFcn', @(createSchedButton,event) createSchedButtonPushed(createSchedButton,jobCountText,matCountText,distParam1Text,distParam2Text));

%show variation coefficient based on distribution parameters
%surf(param1,param2,reshape([prodKPIs(:).throughput],length(param1),length(param2)));

%% ui controls in simPanel
releaseRateText = uieditfield(simPanel,'Position',[10 100 50 20], 'Value','0.2','Tag','releaseRate');
releaseRateLabel = uilabel(simPanel,'Position',[70 100 200 20], 'Text','Release Rate (prod. orders per day)');
simPerSchedText = uieditfield(simPanel,'Position',[10 70 50 20], 'Value','1','Tag','simPerSched');
simPerSchedLabel = uilabel(simPanel,'Position',[70 70 200 20], 'Text','Simulations per schedule');
simSchedButton = uibutton(simPanel,'Position',[10 40 100 20],'Text','Sim Schedules','ButtonPushedFcn', @(simSchedButton,event) simSchedButtonPushed(simSchedButton,scheduleList,schedPanel,prodPanel,evalPanel,releaseRateText,simPerSchedText,simList));

%% callback functions
function reSimButtonPushed(reSimButton,scheduleList,schedPanel,prodPanel,simPanel,statsPanel,countReSims)
%read relevant data for simulatin single schedule
scheduleID = str2num(scheduleList.Value);
countReSims = str2num(countReSims.Value);
tmpSchedule = schedPanel.UserData(scheduleID).schedule;
tmpOperators = prodPanel.UserData.operators;
tmpMachines = prodPanel.UserData.machines;
releaseRate = findobj(simPanel.Children,'Tag','releaseRate');
releaseRate = str2double(releaseRate.Value);
parfor n=1:countReSims
    [WIP, prodKPIs] = simulateProduction(tmpSchedule, tmpOperators, tmpMachines, releaseRate);
    tmpOutWIP(n) = WIP;
    tmpOutKPIs(n) = prodKPIs;
    disp(['sim run: ',num2str(n)]); 
end

%assign values from parfor loop to UserData
for i = 1:countReSims
    schedPanel.UserData(scheduleID).WIP(end+1) = tmpOutWIP(:,i);
    schedPanel.UserData(scheduleID).prodKPIs(end+1) = tmpOutKPIs(:,i);
end

%update list of schedule stats / infos
updateScheduleInfo(scheduleList,statsPanel,schedPanel);

end

function createSchedButtonPushed(createSchedButton,jobCount,matCount,distParam1,distParam2)
schedules = createSchedules(jobCount.Value,matCount.Value,distParam1.Value,distParam2.Value);
createSchedButton.Parent.UserData = schedules;
end

function simSchedButtonPushed(simSchedButton,scheduleList,schedPanel,prodPanel,evalPanel,releaseRate,simPerSched,simList)
simPerSched = str2num(simPerSched.Value);
tmpOperators = prodPanel.UserData.operators;
tmpMachines = prodPanel.UserData.machines;

schedCount = length(schedPanel.UserData(1,:));
tmpSchedules = schedPanel.UserData;
tic
releaseRate = str2double(releaseRate.Value)
parfor i=1:schedCount
    
    disp(['sim sched: ',num2str(i)]);
    for j=1:simPerSched
        
        [WIP, prodKPIs] = simulateProduction(tmpSchedules(i).schedule, tmpOperators, tmpMachines, releaseRate);
        
        tmpOutWIP(i,j) = WIP;
        tmpOutKPIs(i,j) = prodKPIs;       
    end
    
end
toc
fprintf('Executed simulation runs: %d\n',schedCount*simPerSched) 

%count of feasible results
numResults = length(tmpOutWIP);

%assign values to UserData
for i = 1:numResults
    schedPanel.UserData(i).WIP = tmpOutWIP(i,:);
    schedPanel.UserData(i).prodKPIs = tmpOutKPIs(i,:);
end

%update schedule list with number of schedules
scheduleList.Items = string(1:numResults);
scheduleList.Value = {};

%update simList dropdown with number of simulations
simList.Items = string(1:numResults);

end

function drawTP(simList,scheduleList,schedPanel,prodPanel)
tmpOperators = prodPanel.UserData.operators;
scheduleID = str2num(scheduleList.Value);
simID = str2num(simList.Value);
tmpReleaseRate = schedPanel.UserData(scheduleID).prodKPIs(simID).releaseRate;

tmpSchedule = schedPanel.UserData(scheduleID).schedule;

tmpStats = schedPanel.UserData(scheduleID).stats;

tmpWIP = schedPanel.UserData(scheduleID).WIP(simID);
tmpProdKPIs = schedPanel.UserData(scheduleID).prodKPIs(simID);

%create figure to display throughput and histogram
drawThroughput(tmpProdKPIs,tmpWIP,tmpSchedule,tmpStats,tmpOperators,tmpReleaseRate);

%update data in stats panel
% updateScheduleInfo(scheduleList,statsPanel,schedPanel);

end

function saveButtonPushed(schedules,prodKPIs,WIP)
timestamp = datetime('now', 'Format', 'yMMdd-HHmmss');

%save figures
filenameFig = strcat('logs\',string(timestamp),'_Plots.fig');
%savefig(figures,filenameFig,'compact');

%save data
filenameData = strcat('logs\',string(timestamp),'_Data.mat');
save(filenameData,'schedules', 'prodKPIs', 'WIP');
end

function updateScheduleInfo(scheduleList,statsPanel,schedPanel)
id = str2num(scheduleList.Value);

simList = findobj(statsPanel.Children,'Tag','simList');

param1sched = schedPanel.UserData(id).dist.Param1;
param2sched = schedPanel.UserData(id).dist.Param2;

%determine dist params taking mount and unmount time into account
%unmount and mount time is hard-coded for now
auxTime = 2;
actualDist = fitdist(schedPanel.UserData(id).schedule(:,1) + auxTime, 'Gamma');
param1 = actualDist.a;
param2 = actualDist.b;

avg = schedPanel.UserData(id).stats.avg;
std = schedPanel.UserData(id).stats.std;
var = schedPanel.UserData(id).stats.var;
throughputAvg = mean([schedPanel.UserData(id).prodKPIs.throughput]);
throughputMed = median([schedPanel.UserData(id).prodKPIs.throughput]);
opEff = mean([schedPanel.UserData(id).prodKPIs.operatorEfficiency]); 
avgDelay = mean([schedPanel.UserData(id).prodKPIs.avgDelay]); 

simPerSched = length([schedPanel.UserData(id).prodKPIs.throughput]);

n=3;
statsPanel.Children(n+8).Text = strcat("P1: ",sprintf('%.2f',param1)," / ", sprintf('%.2f',param1sched));
statsPanel.Children(n+7).Text = strcat("P2: ",sprintf('%.2f',param2)," / ", sprintf('%.2f',param2sched));
statsPanel.Children(n+6).Text = strcat("Avg. WC: ",sprintf('%.2f',avg));
statsPanel.Children(n+5).Text = strcat("Std. WC: ",sprintf('%.2f',std));
statsPanel.Children(n+4).Text = strcat("Var.: ",sprintf('%.4f',var));
statsPanel.Children(n+3).Text = strcat("TP Avg: ",sprintf('%.4f',throughputAvg));
statsPanel.Children(n+2).Text = strcat("Op. Eff.: ",sprintf('%.4f',opEff));
statsPanel.Children(n+1).Text = strcat("Avg. Delay: ",sprintf('%.2f',avgDelay));
statsPanel.Children(n).Text = strcat("TP Med: ",sprintf('%.4f',throughputMed));
statsPanel.Children(n-1).Text = strcat("n: ",sprintf('%.0u',simPerSched));

simList.Items = string(1:simPerSched);

end

function createProdButtonPushed(createProdButton,prodPanelInput)
%variable definitions
op24 = str2num(prodPanelInput(1).Value);
op08 = str2num(prodPanelInput(2).Value);
op16 = str2num(prodPanelInput(3).Value);

matA = str2num(prodPanelInput(4).Value);
matB = str2num(prodPanelInput(5).Value);
matC = str2num(prodPanelInput(6).Value);

matAMount = str2num(prodPanelInput(7).Value);
matBMount = str2num(prodPanelInput(8).Value);
matCMount = str2num(prodPanelInput(9).Value);
matAUnmount = str2num(prodPanelInput(10).Value);
matBUnmount = str2num(prodPanelInput(11).Value);
matCUnmount = str2num(prodPanelInput(12).Value);
% creation of machines and operator matrix
totalOpCount = op24 + op08 + op16;
totalMachineCount = matA + matB + matC;

createProdButton.Parent.UserData = struct('operators',zeros(totalOpCount,3),'machines',zeros(totalMachineCount,4));

% create operator matrix data
k=1;
if op24 > 0
    for i=1:op24
        operators(k,:) = [0,0,0];
        k=k+1;
    end
end
if op08 > 0
    for i=1:op08
        operators(k,:) = [8,0,0];
        k=k+1;
    end
end
if op16 > 0
    for i=1:op16
        operators(k,:) = [16,0,0];
        k=k+1;
    end
end
createProdButton.Parent.UserData.operators = operators;

% create machine matrix data
k=1;
if matA > 0
    for i=1:matA
        machines(k,:) = [matAMount,matAUnmount,1,0];
        k=k+1;
    end
end
if matB > 0
    for i=1:matB
        machines(k,:) = [matBMount,matBUnmount,2,0];
        k=k+1;
    end
end
if matC > 0
    for i=1:matC
        machines(k,:) = [matCMount,matCUnmount,3,0];
        k=k+1;
    end
end
createProdButton.Parent.UserData.machines = machines;
end