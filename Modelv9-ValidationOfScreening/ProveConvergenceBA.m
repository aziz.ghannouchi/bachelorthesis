n=50
simPerSched = 1:1:n;
simPerSched=simPerSched';
u=zeros(n,2);

op = [2,0,0];                      %Operators
mat= [1,1,1;
      0,1,1;
      0,1,1]';         %Number of Machines, mounting and Unmounting time

%Schedule Creation
matCount = 1;                      %Number of Materials
param1= [20];                 %Distribution Parameter 1
param2= [5];                         %Distribution Parameter 2

releaseRate=0.7;
jobCount=7000*2*releaseRate
[operators,machines]=v3f01_createProd(mat, op)
schedules  = v3f02_createSched(jobCount, matCount, param1, param2)




%% Calculate outputs
for i=1:n
    try
        % First column is the Throughput
        % Second column is the Operator Efficiency
   % [u(i,1),u(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched(i,1));
    
    [ResultWIP,ResultprodKPIs] = v3f03simSched(operators, machines,schedules, simPerSched(i,1), releaseRate);
    
m=length(param1)*length(param2);

tmpThroughput=zeros(simPerSched(i,1),1);
tmpOperatorEfficiency=zeros(simPerSched(i,1),1);

for j=1:simPerSched(i,1)
tmpThroughput(j,1)=ResultprodKPIs(m,j).throughput;
tmpOperatorEfficiency(j,1)=ResultprodKPIs(m,j).operatorEfficiency;
end

% u(i,1) = tmpThroughput(1,1);
% u(i,2)= tmpOperatorEfficiency(1,1);

u(i,1) = mean(tmpThroughput);
u(i,2)= mean(tmpOperatorEfficiency);
    
    catch
        u(i,1)=0;
        u(i,2)=0;
    end
end


figure
scatter(simPerSched,u(:,1))
title('Throughput as a function of simPerSched')
xlabel('simPerSched')
ylabel('Throughput')
ylim([0 1])


figure
scatter(simPerSched,u(:,2))
title('OperatorEfficiency as a function of simPerSched')
xlabel('simPerSched')
ylabel('OperatorEfficiency')
ylim([0 1])


FileName=['ConvergenceSet',datestr(now,'yyyymmddTHHMMSS')];
save(FileName);

%L=u(n,1);
%Delta=0.1;
%i=n;

%while abs(u(i,1)-L)/L < Delta
%i=i-1;
%if i<=0
%break
%end

%end

%n0=i+1;


% 
% function [Throughput, OperatorEfficiency]= AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)
% 
% [operators,machines]=v3f01_createProd(mat, op)
% schedules  = v3f02_createSched(jobCount, matCount, param1, param2)
% 
% [ResultWIP,ResultprodKPIs] = v3f03simSched(operators, machines,...
%     schedules, simPerSched, releaseRate);
% 
% %ResultprodKPIs
% 
% n=length(param1)*length(param2);
% Throughput = ResultprodKPIs(n,1).throughput;
% OperatorEfficiency= ResultprodKPIs(n,1).operatorEfficiency;