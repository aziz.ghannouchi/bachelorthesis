function [Throughput, OperatorEfficiency]= AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)

[operators,machines]=v3f01_createProd(mat, op);
schedules  = v3f02_createSched(jobCount, matCount, param1, param2);

[ResultWIP,ResultprodKPIs] = v3f03simSched(operators, machines,...
    schedules, simPerSched, releaseRate);

%ResultprodKPIs

n=length(param1)*length(param2);
tmpThroughput=zeros(n,1);
tmpOperatorEfficiency=zeros(n,1);

for j=1:n
tmpThroughput(j,1)=ResultprodKPIs(j,1).throughput;
tmpOperatorEfficiency(j,1)=ResultprodKPIs(j,1).operatorEfficiency;
end

Throughput=mean(tmpThroughput);
OperatorEfficiency=mean(tmpOperatorEfficiency);

%Throughput = ResultprodKPIs(n,1).throughput;
%OperatorEfficiency= ResultprodKPIs(n,1).operatorEfficiency;
%disp('Throughput 1 = '); disp(ResultprodKPIs(n,1).throughput)
