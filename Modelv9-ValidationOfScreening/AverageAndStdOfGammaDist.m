% % % % pd = makedist('Gamma','a',2,'b',4)
% % % % pd = 
% % % % 
% % % %   Gamma distribution
% % % %     a = 2     Shape parameter	--> param1
% % % %     b = 4     Scale parameter	--> param2
% % % dist = makedist('Gamma',param1,param2);
% 
distParam1=1:1:144;
distParam2=0.1:1:25;


mean=distParam1.*distParam2';
std=sqrt(distParam1).*distParam2';
interval1=0:25:250;
interval2=100:300:2800;
%% Figure with expected intervals
figure
contour(distParam1,distParam2,std,linspace(1,120,10),'ShowText','on');
%colormap(spring)
hold on
 contour(distParam1,distParam2,mean,linspace(1,60,10),'black','ShowText','on');
 caxis([0 300]);
 xlabel('Distribution Parameter 1 = Shape parameter')
 ylabel('Distribution Parameter 2 = Scale parameter')
 legend('Standard Deviation','Mean')
 title('Mean and standard deviation of work content distribution as a function of the shape and scale parameter')
 
 
 %% Figure with intervals of literature
 figure
contour(distParam1,distParam2,std,interval1,'ShowText','on');
%colormap(spring)
hold on
 contour(distParam1,distParam2,mean,interval2,'black','ShowText','on');
 caxis([0 300]);
 xlabel('Distribution Parameter 1 = Shape parameter')
 ylabel('Distribution Parameter 2 = Scale parameter')
 legend('Standard Deviation','Mean')
 title('Mean and standard deviation of work content distribution as a function of the shape and scale parameter')
 
 
%  mean=distParam1.*distParam2';
%% inverted calculation
 mean2=0:0.5:120;
 std2=0:0.5:60;
 a2= (mean2.^2) ./ (std2.^2)';
 b2= (std2.^2)' ./ (mean2);
 
 %% With expected intervals
 figure
contour(mean2,std2,b2,0.1:2.5:25.1,'blue','ShowText','on');
hold on
  %contour(mean2,std2,a2,1:10:144,'black','ShowText','on');
  contour(mean2,std2,a2,logspace(-1,2.2,10),'black','ShowText','on');
  
  scatter([0.1,14.4,25],[0.1,1.2,25])
  legend('b = Scale Parameter','a=Shape Parameter')
 %caxis([0 300]);
  xlabel('Mean')
 ylabel('Standard Deviation')
 title('Shape and Scale Parameters as a function of the mean and STD')
%  
 
 
 %% with modified intervals for better visibility
  mean2=0:0.01:8;
 std2=0:0.01:3;
 a2= (mean2.^2) ./ (std2.^2)';
 b2= (std2.^2)' ./ (mean2);
 
 figure
contour(mean2,std2,b2,1:2.5:25,'blue','ShowText','on');
hold on
  contour(mean2,std2,a2,logspace(-1,2.2,10),'black','ShowText','on');
    scatter([0.1,14.4],[0.1,1.2])

  legend('b = Scale Parameter','a=Shape Parameter')
 %caxis([0 300]);
  xlabel('Mean')
 ylabel('Standard Deviation')
 title('Shape and Scale Parameters as a function of the mean and STD')
%  