%% Description
% This script plots the generated input samples used for the sensitivity
% analysis


%% Plot all the scatter plots in one figure (Like in Simulink)
figure
for i=1:M
    for j=1:i
            if (i==j)
                subplot(M,M,M*(i-1)+j), histogram(X(:,j))
               % xlabel(XNames[j])
            else
            subplot(M,M,M*(i-1)+j), plot(X(:,i),X(:,j),'.')
           % xlabel(XNames(i))
            %ylabel(XNames(j))
            end
    end
end

%Add Plot labels
for i=1:M
    subplot(M,M,M*(i-1)+1), ylabel(XNames(i)) 
    subplot(M,M,M*(M-1)+i), xlabel(XNames(i))
end

%% All plots separately
for i=1:M
    for j=1:i
        figure
            if (i==j)
                histogram(X(:,j))
                
                xlabel(XNames(j))
            else
            plot(X(:,i),X(:,j),'.')
            xlabel(XNames(i))
            ylabel(XNames(j))
            end
    end
end

%% Plot work content
a=X(:,7);
b=X(:,8);
mu=a.*b;
sigma=sqrt(a).*b;

figure
scatter(mu,sigma,'.')
xlabel('mu [h]')
ylabel('sigma [h]')

%% Plot Work content with contours of values
a=X(:,7);
b=X(:,8);
mu=a.*b;
sigma=sqrt(a).*b;

mean2=0:0.5:120;
 std2=0:0.5:60;
 a2= (mean2.^2) ./ (std2.^2)';
 b2= (std2.^2)' ./ (mean2);
 

figure
scatter(mu,sigma,'.')
xlabel('mu [h]')
ylabel('sigma [h]')

hold on
contour(mean2,std2,b2,0.1:5:25.1,'blue','ShowText','on');
contour(mean2,std2,a2,[1,10,20,50,144],'black','ShowText','on');
  %contour(mean2,std2,a2,logspace(-1,2.2,10),'black','ShowText','on');
  
  scatter([0.1,14.4,25],[0.1,1.2,25])
  legend('Generated inputs for simulation','b= Scale Parameter')