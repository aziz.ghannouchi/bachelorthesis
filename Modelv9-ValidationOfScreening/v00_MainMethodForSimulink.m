function y = fcn(mat, op, jobCount, matCount, param1, param2, simPerSched, releaseRate)

[operators,machines]=v3f01_createProd(mat, op)
schedules  = v3f02_createSched(jobCount, matCount, param1, param2)

[ResultWIP,ResultprodKPIs] = v3f03simSched(operators, machines,schedules, simPerSched, releaseRate);

ResultprodKPIs
n=length(param1)*length(param2);

y = ResultprodKPIs(n,1).throughput;

end

function [operators,machines]=v3f01_createProd(mat, op)
%%variable definitions --->  defined as parameters 

%op is a vector with the length 3
%mat is a 3x3 matrix, so that 
%the first line represents the number of materials of types A, B and C
%the second one the mounting time
%and the 3rd one the unmounting time

op24=op(1);              op08=op(2);                op16=op(3);
matA=mat(1,1);           matB=mat(1,2);             matC=mat(1,3);
matAMount=mat(2,1);      matBMount=mat(2,2);        matCMount=mat(2,3);
matAUnmount=mat(3,1);    matBUnmount=mat(3,2);      matCUnmount=mat(3,3);

% creation of machines and operator matrix
totalOpCount = op24 + op08 + op16;
totalMachineCount = matA + matB + matC;

operators=zeros(totalOpCount,3);
machines= zeros(totalMachineCount,4);

% create operator matrix data
k=1;
if op24 > 0
    for i=1:op24
        operators(k,:) = [0,0,0];
        k=k+1;
    end
end
if op08 > 0
    for i=1:op08
        operators(k,:) = [8,0,0];
        k=k+1;
    end
end
if op16 > 0
    for i=1:op16
        operators(k,:) = [16,0,0];
        k=k+1;
    end
end
ProdOperators = operators;

% create machine matrix data
k=1;
if matA > 0
    for i=1:matA
        machines(k,:) = [matAMount,matAUnmount,1,0];
        k=k+1;
    end
end
if matB > 0
    for i=1:matB
        machines(k,:) = [matBMount,matBUnmount,2,0];
        k=k+1;
    end
end
if matC > 0
    for i=1:matC
        machines(k,:) = [matCMount,matCUnmount,3,0];
        k=k+1;
    end
end
ProdMachines = machines;


end

function schedules  = v3f02_createSched(jobCount, matCount, param1, param2)

% job / schedule parameters
% longestJob = str2num(longestJob);
%param1 --> distParam1 in the control window
%param2 --> distParam2 in the control window
%

%parametrize distribution
param3=1;
%distType = 'Beta';

%%%%clear schedule prodKPIs WIP;
distribution = struct('Type','Gamma','Param1',0,'Param2',0,'Param3',0);
stats = struct('avg',0,'std',0,'var',0);
paramCombinations = uint16(length(param1) * length(param2) * length(param3));
schedule=zeros(jobCount,4);
schedules = struct('schedule',schedule,'distribution',distribution,'stats',stats);

%dist=zeros(paramCombinations,3);
%stats=zeros(paramCombinations,3);
%schedules=zeros(jobCount,4,paramCombinations);

i = 1;
for eachParam1 = param1
    for eachParam2 = param2
        for eachParam3 = param3
            %%%%%i  Aziz commented this out
% %             distribution= createDist(eachParam1,eachParam2,eachParam3);
% %             
% %             schedules(:,:,i)=createSchedule(jobCount,matCount,distribution);
% %             
% %             dist(i,1)=eachParam1;
% %             dist(i,2)=eachParam2;
% %             dist(i,3)=eachParam3;
% %             
% %             stats(i,1)=mean(schedules(:,1,i));
% %             stats(i,2)=std(schedules(:,1,i));
% %             stats(i,3)=stats(i,2)/stats(i,1);
% %             
% %             i=i+1;


%%            
%%                   new version by Aziz
           % schedules = 


            dist = createDist(eachParam1,eachParam2,eachParam3);
            schedules(i).schedule = createSchedule(jobCount,matCount,dist);
           % schedules(i).distribution.Type = distType;
            schedules(i).distribution.Param1 = eachParam1;
            schedules(i).distribution.Param2 = eachParam2;
            schedules(i).distribution.Param3 = eachParam3;
            schedules(i).stats.avg = mean(schedules(1,i).schedule(:,1));
            schedules(i).stats.std = std(schedules(1,i).schedule(:,1));
            schedules(i).stats.var = schedules(i).stats.std / schedules(i).stats.avg;
            i = i+1;




            
            
% % %%            original code written by Tobi
% % %%                   replaced by Aziz
% %             dist = createDist(eachParam1,eachParam2,eachParam3);
% %             schedules(:,i).schedule = createSchedule(jobCount,matCount,dist);
% %             schedules(i).dist.Type = distType;
% %             schedules(i).dist.Param1 = eachParam1;
% %             schedules(i).dist.Param2 = eachParam2;
% %             schedules(i).dist.Param3 = eachParam3;
% %             schedules(i).stats.avg = mean(schedules(1,i).schedule(:,1));
% %             schedules(i).stats.std = std(schedules(1,i).schedule(:,1));
% %             schedules(i).stats.var = schedules(i).stats.std / schedules(i).stats.avg;
% %             i = i+1;
        end
    end
end


%Defines the schedule function used above
function schedule = createSchedule(jobCount,matCount,dist)

schedule = ones(jobCount, 4);

schedule(:,1) = ceil(random(dist,jobCount,1));
%randomly select a material
schedule(:,2) = randi(matCount,jobCount,1);

schedule(:,3) = schedule(:,1);
schedule(:,4) = zeros();

end

%Defines the function to create a distribution
function dist = createDist(param1,param2,param3)
%figure
%skewed normal distribution
% x=-5:0.1:5;
% normDist = @(x) (1/sqrt((2*pi))*exp(-x.^2/2));
% skewNormDist = @(x,alpha) 2*normDist(x).*normcdf(alpha*x);
%plot(x, skewNormDist(x, 6));

%f distribution
% x=0:0.1:5;
% plot(x, fpdf(x, 10,10));

%weibull distribution
%dist = makedist('Weibull',50,2.3);

%burr distribution
%dist = makedist('Burr',param1,param2,param3);

%gamma
%dist = makedist('Gamma',param1,param2);

%lognormal
%dist = makedist('Lognormal',param1,param2);

%uniform distribution
%dist = makedist('Uniform');

%normal distribution (gaussian)
%dist = makedist('Normal',param1,param2);

%weibull distribution
dist = makedist('Gamma',param1,param2);
end



end

function [ResultWIP,ResultprodKPIs] = v3f03simSched(operators, machines,Schedules, simPerSched, releaseRate)

tmpOperators = operators;
tmpMachines = machines;

tmpSchedules = Schedules; 
schedCount = length(Schedules);

%tmpOutWIP       =   zeros(schedCount,simPerSched);
%tmpOutKPIs      =   zeros(schedCount,simPerSched);

parfor i=1:schedCount

    %disp(['sim sched: ',num2str(i)]);
    for j=1:simPerSched
        
        [WIP, prodKPIs] = v3simulateProduction(tmpSchedules(i).schedule, tmpOperators, tmpMachines, releaseRate);
        
        tmpOutWIP(i,j) = WIP;
        tmpOutKPIs(i,j) = prodKPIs;
        
    end
    
end

%fprintf('Executed simulation runs: %d\n',schedCount*simPerSched) 

%count of feasible results
numResults = length(tmpOutWIP);



%assign values to UserData
%for i = 1:numResults           ASK TOBI
 for i = 1:schedCount
   % schedPanel.UserData(i).WIP = tmpOutWIP(i,:);
   % schedPanel.UserData(i).prodKPIs = tmpOutKPIs(i,:);
    
   ResultWIP(i,:) = tmpOutWIP(i,:);
   ResultprodKPIs(i,:) = tmpOutKPIs(i,:);
  
  
  
    
    
end

%update schedule list with number of schedules
%scheduleList.Items = string(1:numResults);
%scheduleList.Value = {};

%update simList dropdown with number of simulations
%simList.Items = string(1:numResults);

end

function dist = createDist(param1,param2,param3)
%figure
%skewed normal distribution
% x=-5:0.1:5;
% normDist = @(x) (1/sqrt((2*pi))*exp(-x.^2/2));
% skewNormDist = @(x,alpha) 2*normDist(x).*normcdf(alpha*x);
%plot(x, skewNormDist(x, 6));

%f distribution
% x=0:0.1:5;
% plot(x, fpdf(x, 10,10));

%weibull distribution
%dist = makedist('Weibull',50,2.3);

%burr distribution
%dist = makedist('Burr',param1,param2,param3);

%gamma
%dist = makedist('Gamma',param1,param2);

%lognormal
%dist = makedist('Lognormal',param1,param2);

%uniform distribution
%dist = makedist('Uniform');

%normal distribution (gaussian)
%dist = makedist('Normal',param1,param2);

%weibull distribution
dist = makedist('Gamma',param1,param2);
end

function schedule = createSchedule(jobCount,matCount,dist)

schedule = ones(jobCount, 4);

schedule(:,1) = ceil(random(dist,jobCount,1));
%randomly select a material
schedule(:,2) = randi(matCount,jobCount,1);

schedule(:,3) = schedule(:,1);
schedule(:,4) = zeros();

end

function schedules = createSchedules(jobCount,matCount,distParam1,distParam2)
%define job / schedule parameters
% longestJob = str2num(longestJob);
jobCount = str2num(jobCount);
matCount = str2num(matCount)

%parametrize distribution
param1=str2num(distParam1);
param2=str2num(distParam2);
param3=1;
distType = 'Gamma';

clear schedule prodKPIs WIP;
dist = struct('Type',distType,'Param1',0,'Param2',0);
stats = struct('avg',0,'std',0,'var',0);
schedules = struct('schedule',cell(1,1),'dist',dist,'stats',stats);
paramCombinations = uint16(length(param1) * length(param2) * length(param3));

i = 1;
for eachParam1 = param1
    for eachParam2 = param2
        for eachParam3 = param3
            %pre-calculate avg depending on param1 and param2 of Gamma dist
            avg = eachParam1 * eachParam2;
            if avg < 120
                dist = makedist(distType,eachParam1,eachParam2);
                i 
                schedules(:,i).schedule = createSchedule(jobCount,matCount,dist);
                schedules(i).dist.Type = distType;
                schedules(i).dist.Param1 = eachParam1;
                schedules(i).dist.Param2 = eachParam2;
                %             schedules(i).dist.Param3 = eachParam3;
                schedules(i).stats.avg = mean(schedules(1,i).schedule(:,1));
                schedules(i).stats.std = std(schedules(1,i).schedule(:,1));
                schedules(i).stats.var = schedules(i).stats.std / schedules(i).stats.avg;
                i = i+1;
            end
        end
    end
end
end

function [WIP,prodKPIs] = v3simulateProduction(jobs,operators,machines,releaseRate)

WIP = struct('started',cell(1,1),'completed',cell(1,1),'queued',cell(1,1));
jobs = [jobs,zeros(length(jobs(:,1)),3)];

completed = [];
started = [];

%convert release rate of production order from daily to hourly
%release new job for production (data prep finished) at a rate of
%releaseRate (measured in jobs per hour and machine
releaseRate = releaseRate / 24;

%counting variable for indexing started and completed jobs
c = 1;
d = 1;

%start simulation with 0800 shift
shift = 8;
n=1;

%create queue and start with randomly selected job
jobID = jobs(randi(length(jobs(1,:))));
queue(1,:) = jobs(jobID,:);
queue(1,8) = 0;

%calculate throughput time depending on process time and
%material-/machine-specific mount-/unmount-time
matIndex = find(machines(:,3) == jobs(jobID,2),1);
wcJob = jobs(jobID,1) + sum(machines(matIndex,1:2));

%add selected job to queue history
queueHist = [d,0,wcJob];
d = d+1;
%remove selected (queued) job from joblist
jobs(jobID,:) = [];

%define machine availability
availability = 1;

%run production for 3840 hours (160 days)
timeframe=3840*2;

%create evenly distributed random values to simulate machine downtime
%randValue = rand(timeframe, length(jobs));


%simulate production hour by hour
for k = 1:timeframe

    %current working hour of shift
    n = mod(k,8)+1;
    
    %release new job at rate defined above
    if rand(1) < releaseRate
        jobID = jobs(randi(length(jobs(1,:))));
        %add job to queue
        queue = [queue; [jobs(jobID,:),0]];
        
        %calculate throughput time depending on process time and
        %material-/machine-specific mount-/unmount-time
        matIndex = find(machines(:,3) == jobs(jobID,2),1);
        wcJob = jobs(jobID,1) + sum(machines(matIndex,1:2));
        
        %add selected job to queue history
        queueHist = [queueHist; [d, k, wcJob]];
        d=d+1;

        %remove selected (queued) job from joblist
        jobs(jobID,:) = [];
    end
    %in case no job is queued (i.e. waiting for or in production) -> skip
    %to next hour
    if isempty(queue)
        continue
    end
       
    %calculate if 8, 16 or 0 h shift
    if mod(k,8) == 0 && shift < 24
        shift = shift + 8;
    elseif shift == 24
        shift = 0;
    end
    
    %check for available operators
    availableOperators = [];
    for m=1:length(operators(:,1))
        if operators(m,2) == 0 && shift == operators(m,1) && operators(m,3) ~= k
            availableOperators = [availableOperators; m];
        end
    end
    
    %check for available machines
    availableMachines = [];
    n=1;
    for j = 1:length(machines(:,1))
        if machines(j,4) == 0 
            availableMachines = [availableMachines; j];
            n=n+1;
        end
    end
    
    %iterate through queued jobs 
    i=1;
    while (i <= size(queue,1))

        %take machine unavailability into account
        %if randValue(k,i) < (1-availability)
        %    continue
        %end
        
        %check for duration and material type of current job
        jobMaterial = queue(i,2);
        
        %check if job material fits material capabilities of available
        %machines
        machineAvailable = ismember(jobMaterial,machines(availableMachines,3));
        
        %check if operator is available
        operatorAvailable = ~isempty(availableOperators);
        if operatorAvailable
            selectedOperator = availableOperators(1);
        end
        
        %start job
        if queue(i,1) == queue(i,3) && queue(i,4) == 0 && operatorAvailable && machineAvailable
            %select 1st machine from list of available machines
            selectedMachine = availableMachines(1,1);
                    
            %operator may only start job if his shift lasts longer than
            %required mounting time
            if 8-n >= machines(selectedMachine,1)
                
                %assign operator to selected machine and queued job
                operators(selectedOperator,2) = selectedMachine;
                queue(i,5) = selectedOperator;
                
                %remove operator from list of available operators
                availableOperators(1) = [];
                
                %assign selected machine to job i in queue
                queue(i,4) = selectedMachine;
                
                %remove selected machine from list of available
                %machines
                availableMachines(1,:) = [];
                     
                %read machine-specific mount- and unmount-time from machines list
                queue(i,6) = machines(selectedMachine,1);
                queue(i,7) = machines(selectedMachine,2);
                
                %block selected machine after start of job
                machines(selectedMachine,4) = 1;
                
                %add mount- and unmount-time to actual job duration to
                %determine throughput time
                throughputTime = queue(i,1) + machines(selectedMachine,1) + machines(selectedMachine,2);
                
                started = [started; [c throughputTime k]];
                queue(i,8) = c;
                c=c+1;
                clear throughputTime;
            end
        end
        
        %mounting procedure ongoing
        if queue(i,6) > 0
            queue(i,6) = queue(i,6) - 1;

            %keep operator blocked during mounting procedure
            operators(selectedOperator,3) = k;
            %release operator (only for operator's currently assigned
            %machine) if mounting procedure is finished
            if queue(i,6) == 0
                operators(selectedOperator,2) = 0;
            end
            
        %job running & mounting finished
        elseif queue(i,3) > 0 && queue(i,4) > 0
            queue(i,6) = -1;
            queue(i,3) = queue(i,3) - 1;      
            
        %job completed & unmount ongoing
        elseif queue(i,3) == 0 && queue(i,7) > 0 && operatorAvailable && 8-n >= jobs(i,7) 
            queue(i,7) = queue(i,7) - 1;
            operators(selectedOperator,3) = k;
            availableOperators(1) = [];
            if queue(i,7) == 0
                queue(i,3) = -1;
            end          

        %unmount completed job
        elseif queue(i,7) == 0 && queue(i,3) == -1 && queue(i,4) > 0
            machines(queue(i,4),4) = 0;
            queue(i,7) = -1;
            %calculate throughput time including mount and unmount
            throughputTime = queue(i,1) + machines(queue(i,4),1) + machines(queue(i,4),2);
            
            %save jobID, throughput-time, finish period and ID of used machine
            completed = [completed; [queue(i,8) throughputTime k queue(i,4)]];
            
            %remove completed job from queue
            queue(i,:) = [];
            clear throughputTime;
        end
     i=i+1;   
    end
   
end

WIP.started = started
WIP.completed = completed
WIP.queued = queueHist

prodKPIs = evaluateProduction(started,completed,queueHist,operators,machines,releaseRate * 24);

end



function prodKPIs = evaluateProduction(started, completed,queueHist, operators, machines, releaseRate)
clear throughputTime;
clear operatorEfficiency;
prodKPIs = struct('throughput',0,'operatorEfficiency',0,'avgDelay',0);



for i=1:length(completed(:,1))
 %for i=1:length(completed)
    %actual time from production release to completion (including mounting and
    %unmount time)
    %throughputTime(i,1) = completed(i,3) - started(find(started(:,1)==completed(i,1)),3);
    throughputTime(i,1) = completed(i,3) - queueHist(find(queueHist(:,1)==completed(i,1)),2);
    
    %work content of completed job (including mounting and unmount
    %time)
    throughputTime(i,2) = completed(i,2);
    
 end

avgDelay = mean(throughputTime(:,1) - throughputTime(:,2));

%get throughput from slope of linear fit through all datapoints
pp=polyfit(single(completed(1:length(completed(:,1)),3)), single(cumsum(completed(1:length(completed),2))), 1);
throughput = pp(1);

%calculate operator utilization
countOperators = length(operators(:,1)) * 8 / 24;
totalWorktime = (max(completed(:,3)) - mod(max(completed(:,3)),24)) * countOperators + mod(max(completed(:,3)),24) * countOperators;

totalProductiveTime = 0.0;
for i=1:length(completed(:,1))
    totalProductiveTime = totalProductiveTime + machines(completed(i,4),1) + machines(completed(i,4),2);
end
operatorEfficiency = double(totalProductiveTime) / double(totalWorktime);

prodKPIs.throughput = throughput;
prodKPIs.operatorEfficiency = operatorEfficiency;
prodKPIs.avgDelay = avgDelay;
%prodKPIs.releaseRate = releaseRate;        why is this here????
end



