% This workflow scripts demonstrates the approximation strategy 
% of PAWN indices from generic input-output dataset
% by application to the Ishigami-homma function, 
% as described in Section 3.1 of:
%
% Pianosi, F. and Wagener, T. (2018), Distribution-based sensitivity 
% analysis from a generic input-output sample, Env. Mod. & Soft.
%
% The code works together with the SAFE Toolbox. More information and
% dowload at: www.bris.ac.uk/cabot/resources/safe-toolbox/
%
% This code developed by Francesca Pianosi, University of Bristol
% email: francesca.pianosi@bristol.ac.uk

%%%%%%%%%%%%%%%%%%%%% SET PATH %%%%%%%%%%%%%%%%%%%%%

% Set the path to the SAFE Toolbox directory
% (where you must have also copied the additional functions/data 
% for applying PAWN to generic datasets)

my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir)
addpath(genpath(my_dir))

X_Labels={    'MachineA',    'MountingTimeA',    'UnmountingTimeA',    'Operator0',   'Operator8',   'Operator16',  'DistParam1',  'DistParam2',  'ReleaseRate'};

n=4;
nboot=1000;
%% Calculate the indices
[KS_medianTP,KS_meanTP,KS_maxTP,KS_dummyTP,YYTP,xcTP,NCTP,XXTP] = pawn_indices_givendata(X,Y(:,1),n,nboot);
[KS_medianOE,KS_meanOE,KS_maxOE,KS_dummyOE,YYOE,xcOE,NCOE,XXOE] = pawn_indices_givendata(X,Y(:,2),n,nboot);

%% Calculate statistics across bootstrap resamples
alfa= 0.05;
Spawn_m=mean(KS_mean);
KS_sort=sort(KS_mean);
Spawn_lb = KS_sort(max(1,round(Nboot*alfa/2)),:);
Spawn_ub = KS_sort(round(Nboot*(1-alfa/2)),:);

%% Plot the PAWN sensitivity indices with the condifence intervals
figure
boxplot1(Spawn_m,X_Labels,'Sensitivity Index (PAWN)',Spawn_lb,Spawn_ub)
