% 
%Load X and Y along with the vector idxb that represents the indices that
%lead to the region of interest

%% Define Xnew and Ynew for the output region of interest
indices=[];
k=0;
for i=1:length(X)
    if idxb(i,1)==1
        k=k+1;
        indices(k,1)=i;
    end
end

Xnew=X(indices,:);
Ynew=Y(indices,:);

%% Plot only the output region of interest
figure; scatter_plots(Xnew,Ynew(:,1),[],'Throughput',X_Labels) ;    
figure; scatter_plots(Xnew,Ynew(:,2),[],'OperatorEfficiency',X_Labels) ;


%% Perform PAWN
n=4;
nboot=1000;
%% Calculate the indices
[KS_medianTP,KS_meanTP,KS_maxTP,KS_dummyTP,YYTP,xcTP,NCTP,XXTP] = pawn_indices_givendata(Xnew,Ynew(:,1),n,nboot);
[KS_medianOE,KS_meanOE,KS_maxOE,KS_dummyOE,YYOE,xcOE,NCOE,XXOE] = pawn_indices_givendata(Xnew,Ynew(:,2),n,nboot);

%% Calculate statistics across bootstrap resamples
alfa= 0.05;
Spawn_mTP=mean(KS_meanTP);
KS_sortTP=sort(KS_meanTP);
Spawn_lbTP = KS_sortTP(max(1,round(Nboot*alfa/2)),:);
Spawn_ubTP = KS_sortTP(round(Nboot*(1-alfa/2)),:);

%% Plot the PAWN sensitivity indices with the condifence intervals
figure
boxplot1(Spawn_mTP,X_Labels,'Sensitivity Index TP (PAWN)',Spawn_lbTP,Spawn_ubTP)
