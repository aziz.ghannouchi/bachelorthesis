function [operators,machines]=v3f01_createProd(mat, op)
%%variable definitions --->  defined as parameters 

%op is a vector with the length 3
%mat is a 3x3 matrix, so that 
%the first line represents the number of materials of types A, B and C
%the second one the mounting time
%and the 3rd one the unmounting time

%      mat=[XA(i,1)    XA(i,2)   XA(i,3)
%           0         1           1
%           0         1           1]'


op24=op(1);              op08=op(2);                op16=op(3);

matA=mat(1,1);           matB=mat(1,2);             matC=mat(1,3);
matAMount=mat(2,1);      matBMount=mat(2,2);        matCMount=mat(2,3);
matAUnmount=mat(3,1);    matBUnmount=mat(3,2);      matCUnmount=mat(3,3);

% creation of machines and operator matrix
totalOpCount = op24 + op08 + op16;
totalMachineCount = matA + matB + matC;

operators=zeros(totalOpCount,3);
machines= zeros(totalMachineCount,4);

% create operator matrix data
k=1;
if op24 > 0
    for i=1:op24
        operators(k,:) = [0,0,0];
        k=k+1;
    end
end
if op08 > 0
    for i=1:op08
        operators(k,:) = [8,0,0];
        k=k+1;
    end
end
if op16 > 0
    for i=1:op16
        operators(k,:) = [16,0,0];
        k=k+1;
    end
end
ProdOperators = operators;

% create machine matrix data
k=1;
if matA > 0
    for i=1:matA
        machines(k,:) = [matAMount,matAUnmount,1,0];
        k=k+1;
    end
end
if matB > 0
    for i=1:matB
        machines(k,:) = [matBMount,matBUnmount,2,0];
        k=k+1;
    end
end
if matC > 0
    for i=1:matC
        machines(k,:) = [matCMount,matCUnmount,3,0];
        k=k+1;
    end
end
ProdMachines = machines;


end