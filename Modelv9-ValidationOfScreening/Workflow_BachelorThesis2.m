%% Description
%This is the main workflow for the global sensitivity analysis performed on
%the simulation that modelizes a production plant with Laser Powder Bed
%Fusion

%This script was written by Aziz Ghannouchi in the context of the bachelor
%thesis "Sensitivity analysis of a simulation model to determine the key   
%performance indicators for laser powder bed fusion", under the supervision
%of Tobias Stittgen at the insitute of Digital Additive Production DAP of
%RWTH Aachen University.

logname=['logOutputCalculationBA',datestr(now,'yyyymmddTHHMMSS')];
diary(logname)
diary ON

%% Step 1: set paths

my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir);
addpath(genpath(my_dir));



%% Step 2: setup the model and define input ranges
% Define input distribution and ranges:
%Parameters: Machine availability, Operator presence, Work Distribution,
%Release rate



%% Sampling strategy
% samp_strat = 'rsu'      %Random uniform
samp_strat = 'lhs'      %Random uniform

%% Names of the parameters
XNames=[    "Machine A",    "MountingTimeA",    "UnmountingTimeA",   "Operator0",   "Operator8",   "Operator16",    "DistParam 1",  "DistParam 2",  "ReleaseRate"]

%% Distribution of the parameters; unid = uniform discrete, unif= uniform
distr_fun={ 'unid',         'unid',             'unid',              'unid',        'unid',         'unid',         'unif',         'unif',         'unif'}


%% Ranges of the parameters
distr_par={ 10,              4,                  4,                      6,              6,              6,     [1 144]    ,   [0.1 25]   	,   [0.1 12]}

disp('DistParam1:')
cell2mat(distr_par(7))
disp('DistParam2:')
cell2mat(distr_par(8))
disp('Release Rate:')
cell2mat(distr_par(9))



%% Number of parameters, given as the length of the vector
M=length(XNames);

%% Number of generated samples
%Note that this is not the number of simulations performed
%Because of the "Resampling", the total number of simulations is equal to
%N*0.5*(M+2)
N=200;


%% Generate the samples
 rng('shuffle')
% X = AAT_sampling_BAAziz(samp_strat,M,distr_fun,distr_par,N);
X = AAT_sampling_BAAziz2(samp_strat,M,distr_fun,distr_par,2*N);

%'unid' generates a random number between 1 and a max value
%--> Xtmp substracts one from the generated value to get a number between 0 and max-1
Xtmp=[zeros(2*N,3) ones(2*N,3) zeros(2*N,3)];
X=X-Xtmp;



%% Resampling
%This steps later allows to find the total sensitivity indices
[ XA, XB, XC ] = vbsa_resampling(X) ;


%% Calculate the results for XA 
NA= size(XA);
NA=NA(1)
YA=zeros(NA,2);
tic
disp('Calculating YA:')
parfor i=1:NA
%Assign values from the generated matrix to the variables of the simulation
      mat=[XA(i,1)  XA(i,2)    XA(i,3)
           0        1          1 
           0        1          1]'       

     op=[XA(i,4)    XA(i,5)    XA(i,6)];
               %  op=[1    1    1];
     matCount=1;
     param1=XA(i,7);
     param2=XA(i,8) ;
     releaseRate=XA(i,9);
          jobCount=round(7000*2*releaseRate) ; %jobCount=1000 
     simPerSched=30;
 
       %     Assign the outputs to the results matrix
       %     YA(i,1) --> Throughput
       %     YA(i,2) --> OperatorEfficiency
   try
% % % % % %       [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
%        [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs([XA(i,1)  XA(i,2)    XA(i,3); 0 1 1;0 1 1],[XA(i,4)    XA(i,5)    XA(i,6)],round(7000*2*XA(i,9)),1,XA(i,7),XA(i,8),XA(i,9),30);
       [a,b]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
YA(i,:)=[a,b];

%YA(i,1)=YA(i,1)/XA(i,1);
  catch
   YA(i,:)=[0,0];
   end
      disp(['YA(',num2str(i),') = '] )
      disp(YA(i,:))

end
YA(:,1)=YA(:,1)./XA(:,1);
YA
toc

%% Calculate the results for XB 
NB= size(XB);
NB=NB(1)
YB=zeros(NB,2);
tic
disp('Calculating YB:')
parfor i=1:NB
%Assign values from the generated matrix to the variables of the simulation
      mat=[XB(i,1)  XB(i,2)    XB(i,3)
           0        1          1 
           0        1          1]'       

     op=[XB(i,4)    XB(i,5)    XB(i,6)];
               %  op=[1    1    1];
     matCount=1;
     param1=XB(i,7);
     param2=XB(i,8) ;
     releaseRate=XB(i,9);
          jobCount=round(7000*2*releaseRate) ; %jobCount=1000 
     simPerSched=30;
 
       %     Assign the outputs to the results matrix
       %     YA(i,1) --> Throughput
       %     YA(i,2) --> OperatorEfficiency
   try
% % % % % %       [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
%        [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs([XA(i,1)  XA(i,2)    XA(i,3); 0 1 1;0 1 1],[XA(i,4)    XA(i,5)    XA(i,6)],round(7000*2*XA(i,9)),1,XA(i,7),XA(i,8),XA(i,9),30);
       [a,b]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
YB(i,:)=[a,b];

%YA(i,1)=YA(i,1)/XA(i,1);
  catch
   YB(i,:)=[0,0];
   end
 disp(['YB(',num2str(i),') = '] )
      disp(YB(i,:))
end
YB(:,1)=YB(:,1)./XB(:,1);
YB
toc


%% Calculate the results for XC 
NC= size(XC);
NC=NC(1)
YC=zeros(NC,2);
tic
disp('Calculating YC:')
parfor i=1:NC
%Assign values from the generated matrix to the variables of the simulation
      mat=[XC(i,1)  XC(i,2)    XC(i,3)
           0        1          1 
           0        1          1]'       

     op=[XC(i,4)    XC(i,5)    XC(i,6)];
               %  op=[1    1    1];
     matCount=1;
     param1=XC(i,7);
     param2=XC(i,8) ;
     releaseRate=XC(i,9);
          jobCount=round(7000*2*releaseRate) ; %jobCount=1000 
     simPerSched=30;
 
       %     Assign the outputs to the results matrix
       %     YA(i,1) --> Throughput
       %     YA(i,2) --> OperatorEfficiency
   try
% % % % % %       [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
%        [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs([XA(i,1)  XA(i,2)    XA(i,3); 0 1 1;0 1 1],[XA(i,4)    XA(i,5)    XA(i,6)],round(7000*2*XA(i,9)),1,XA(i,7),XA(i,8),XA(i,9),30);
       [a,b]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
YC(i,:)=[a,b];

%YA(i,1)=YA(i,1)/XA(i,1);
  catch
   YC(i,:)=[0,0];
   end
 disp(['YC(',num2str(i),') = '] )
      disp(YC(i,:))
end
YC(:,1)=YC(:,1)./XC(:,1);
YC
toc


%% Save workflow for further use
% saves the workspace for further use: to perform the next steps of the
% sensitivity analysis
FileName=['Workspace',datestr(now,'yyyymmddTHHMMSS')]
save(FileName)
diary OFF
