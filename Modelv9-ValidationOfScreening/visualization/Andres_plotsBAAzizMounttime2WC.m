function Andres_plots(X1,X2,X3,Y1,Y2,Y3,idx) 
% 
% This function produces the sensitivity analysis plots first proposed by
% Andres (1997) and used for instance by Tang et al. (2007)
% to assess whether an input has no influence on the output.
%
% These are two scatter plots comparing three sets of output samples:
% Y: output samples generated by varying all inputs (set 'Y')
% Y1: output samples generated by varying all inputs but the i-th
% Y2: output samples generated by varying only the i-th input
%
% If the scatter plot (Y,Y1) aligns along the bisector and
% the scatter plot (Y,Y2) aligns along a horizontal line,
% this suggest that the i-th input is non-influential.
%
% Usage:
% Andres_plots(X,Y,X_ref,i,fun_test) 
%
%      X = set of input samples                              - matrix (N,M)
%      Y = set of output samples                             - vector (N,1)
%  X_ref = reference values for the inputs when not varying  - vector (1,M)
%      i = index of input to be analyzed                           - scalar
% fun_test = name of the function implementing the model: Y=f(X)   - string
%
% References:
%
% Andres, T.H. (1997). Sampling methods and sensitivity analysis for 
% large parameter sets. Journal of Statistical Computation and Simulation, 
% 57(1-4), 77--110.
%
% Tang, Y., Reed, P., Wagener, T., and van Werkhoven, K. (2007). Comparing 
% sensitivity analysis methods to advance lumped watershed model 
% identification and evaluation, Hydrol. Earth Syst. Sci., 11, 793-817.

% This function is part of the SAFE Toolbox by F. Pianosi, F. Sarrazin 
% and T. Wagener at Bristol University (2015). 
% SAFE is provided without any warranty and for non-commercial use only. 
% For more details, see the Licence file included in the root directory 
% of this distribution.
% For any comment and feedback, or to discuss a Licence agreement for 
% commercial use, please contact: francesca.pianosi@bristol.ac.uk
% For details on how to cite SAFE in your publication, please see: 
% bristol.ac.uk/cabot/resources/safe-toolbox/

% Options for the graphic:
fn = 'Helvetica' ; % font type of axes, labels, etc.
%fn = 'Courier' ;
fs = 20 ; % font size of axes, labels, etc.

% [N,M]=size(X1) ;
% [n,m]=size(Y1) ;
% if N~=n; error('input ''X'' and ''Y'' must have the same number of rows'); end
% if m~=1; error('input ''Y'' must be a column vector'); end
% % [n,m]=size(X_ref) ;
% if M~=m; error('Input arguments ''X'' and ''X_ref'' must have the same number of columns'); end
% if n~=1; error('Input argument ''X_ref'' must be a row vector'); end
% if ~isscalar(idx); error('Input argument ''idx'' must be an integer scalar'); end
% if ((idx)-floor(idx)); error('Input argument ''idx'' must be an integer scalar'); end
% if idx>M; error('Input argument ''idx'' exceeds the number of columns in X'); end
%     
% X_1 = X ; X_1(:,idx) = X_ref(idx) ;
% X_2 = repmat(X_ref,N,1) ; X_2(:,idx) = X(:,idx) ;
% Y_1 = nan(N,1) ;
% Y_2 = nan(N,1) ;

WC1=X1(:,7).*X1(:,8);
WC2=X2(:,7).*X2(:,8);
WC3=X3(:,7).*X3(:,8);

[WC1sorted,E1]=sort(WC1);

% [WC2sorted,E2]=sort(WC2);
% [WC3sorted,E3]=sort(WC3);

WC1arranged=WC1(E1);
WC2arranged=WC2(E1);
WC3arranged=WC3(E1);
Y10=Y1(E1);
Y20=Y2(E1);
Y30=Y3(E1);

n=5;


%% normal figure
% figure
mY = 0 ;
MY = 1.1 ;
% %
% 
% subplot(121)
% plot(Y1,Y1,'b')
% hold on
% scatter(Y1,Y2,[],totalTime2,'filled')
% axis([mY,MY,mY,MY])
% xlabel('Y1 (all varying)','FontSize',fs,'FontName',fn)
% ylabel(['Y2 (only non-influential parameters varying)'],'FontSize',fs,'FontName',fn)
% set(gca,'FontSize',fs,'FontName',fn)
% axis square
% %
% subplot(122)
% plot(Y1,Y1,'b')
% hold on
% scatter(Y1,Y3,[],totalTime1,'filled')
% axis([mY,MY,mY,MY])
% xlabel('Y1 (all varying)','FontSize',fs,'FontName',fn)
% ylabel(['Y3 (only influential parameters varying)'],'FontSize',fs,'FontName',fn)
% set(gca,'FontSize',fs,'FontName',fn)
% axis square
% 
% colormap(jet)

%% Figure with WC stepwise
figure
mY = 0 ;
MY = 1.1 ;
%
subplot(121)
plot(Y1,Y1,'b','HandleVisibility','off')
hold on

for i=1:5
    range=[(i-1)*400+1 :(i)*400];
    name=['WC between ', num2str(round(WC1arranged((i-1)*400+1))), ' and ', num2str(round(WC1arranged((i)*400)))];
scatter(Y10(range),Y20(range),4,'filled','DisplayName',name)
disp(max(Y10(range)))
end

axis([mY,MY,mY,MY])
xlabel('Y1 (all varying)','FontSize',fs,'FontName',fn)
ylabel(['Y2 (only non-influential parameters varying)'],'FontSize',fs,'FontName',fn)
set(gca,'FontSize',fs,'FontName',fn)
axis square
legend
%
subplot(122)
plot(Y1,Y1,'b')
hold on

for i=1:5
    range=[(i-1)*400+1 :(i)*400];
%         name=['WC between ', num2str(WC1arranged((i-1)*400+1)), ' and ', num2str(WC1arranged((i)*400))];

scatter(Y10(range),Y30(range),4,'filled')
end
axis([mY,MY,mY,MY])
xlabel('Y1 (all varying)','FontSize',fs,'FontName',fn)
ylabel(['Y3 (only influential parameters varying)'],'FontSize',fs,'FontName',fn)
set(gca,'FontSize',fs,'FontName',fn)
axis square

colormap(jet)

