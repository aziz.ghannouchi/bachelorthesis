%% Step 1: set paths

my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir);
addpath(genpath(my_dir));

%% Load Data and define the three data sets to be used for the Andres plots
% Only Y(:,1) because we are only focussing on the throughput
% Set1=load('');
Set2=load('Workspace20200923T044537SamplingNONInfluential');
Set3=load('Workspace20200923T003508SamplingINFLUENTIAL') ; 
    
% X1=Set1.X;  Y1=Set1.Y(:,1);
X2=Set2.X;  Y2=Set2.Y(:,1);
X3=Set3.X;  Y3=Set3.Y(:,1);

idx=[2 3 4 5 6]; % Indices of the variables to be fixed

Andres_plots(X1,X2,X3,Y1,Y2,Y3,idx);