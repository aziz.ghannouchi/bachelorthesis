%% Step 1: set paths

my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir)
addpath(genpath(my_dir))



%% Step 2: setup the model and define input ranges
% Define input distribution and ranges:
%Parameters: Machine availability, Operator presence, Work Distribution,
%Release rate

%Machine availability: 3, uniform, discrete between 0 and 5?
%Operator Presence: 3, uniform, discrete between 0 and 5?
%Work Distribution: 2, Form and scale parameters, Uniform?
%Release Rate: 1, Uniform?

%Number of total parameters: 9
samp_strat = 'rsu'  ;    %Random uniform
%M = 11;

% % % XNames=[    "Machine A",    "MountingTime",    "UnmountingTime",    "Operator0",   "Operator8",   "Operator16",  "DistParam 1",  "DistParam 2",  "ReleaseRate",  "jobCount", "matCount"];
% % % distr_fun={ 'unid',         'unid',             'unid',             'unid',         'unid',         'unid',         'unif',         'unif',         'unif',         'unid',     'unid'};
% % % distr_par={ 10,              4,                  4,                 5,              5,              5,              [2.8 3.2],      [2.8 3.2]	,   [0.2 0.5],       801,      2};
% % % 

XNames=[    "Machine A",    "MountingTimeA",    "UnmountingTimeA",  "MachineB", "MountingTimeB", "UnmountingTimeB", "Machine C",    "MountingTimeC",    "UnmountingTimeC",   "Operator0",   "Operator8",   "Operator16",  "DistParam 1",  "DistParam 2",  "ReleaseRate",  "jobCount", "matCount"];
distr_fun={ 'unid',         'unid',             'unid',            'unid',      'unid' ,          'unid',         'unid',           'unid',            'unid',               'unid',        'unid',         'unid',         'unif',         'unif',         'unif',         'unid',     'unid'};
M=length(XNames);
distr_par={ 10,              4,                  4,                 10,         4,              4,                  10,             4                    4                      5,              5,              5,         [2.8 3.2],      [2.8 3.2]	,   [0.2 0.5],       801,      2};
N=10;


%'unid' generates a random number between 1 and a max value
%--> Xtmp substracts one from the generated value to get a number between 0 and max-1

X = AAT_sampling(samp_strat,M,distr_fun,distr_par,N);

%Xtmp = [ones(N,6) zeros(N,3) ones(N,1) zeros(N,1)];
Xtmp=[zeros(N,M-2) 200*ones(N,1) zeros(N,1)];
X=X+Xtmp;


%plot all the scatter plots in one figure (Like in Simulink)
% % % % figure
% % % % for i=1:M
% % % %     for j=1:i
% % % %             if (i==j)
% % % %                 subplot(M,M,M*(i-1)+j), histogram(X(:,j))
% % % %                % xlabel(XNames[j])
% % % %             else
% % % %             subplot(M,M,M*(i-1)+j), plot(X(:,i),X(:,j),'.')
% % % %            % xlabel(XNames(i))
% % % %             %ylabel(XNames(j))
% % % %             end
% % % %     end
% % % % end

% % % % %Add Plot labels
% % % % for i=1:M
% % % %     subplot(M,M,M*(i-1)+1), ylabel(XNames(i)) 
% % % %     subplot(M,M,M*(M-1)+i), xlabel(XNames(i))
% % % % end

% % %All plots separately
% % for i=1:11
% %     for j=1:i
% %         figure
% %             if (i==j)
% %                 histogram(X(:,j))
% %             else
% %             plot(X(:,i),X(:,j),'.')
% %  xlabel(XNames(i))
% %             ylabel(XNames(j))
% %             end
% %     end
% % end

[ XA, XB, XC ] = vbsa_resampling(X) ;

%length of the matrices XA 
N= size(XA)
N=N(1)


%Calculating results for XA
for i=1:N
%Assign values from the generated matrix to the variables of the simulation
      mat=[XA(i,1)  XA(i,2)    XA(i,3)
          XA(i,4)   XA(i,5)     XA(i,6)
          XA(i,7)   XA(i,8)     XA(i,9)]'       
%                        mat=[XA(i,1)         1           1    %set different machine data
%                             0         1           1
%                             0         1           1]'

     op=[XA(i,10)    XA(i,11)    XA(i,12)];
     
               %  op=[1    1    1];
     jobCount=XA(i,16) ; %jobCount=1000 
     %%%matcount is set to one!!
     matCount=XA(i,17);  matCount=1;
     param1=XA(i,13);
     param2=XA(i,14) ;
     releaseRate=XA(i,15);
     simPerSched=50;
     
       %     Assign the outputs to the results matrix
       %     YA(i,1) --> Throughput
       %     YA(i,2) --> OperatorEfficiency
       
    try
      [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)
    catch
     YA(i,1)=0;
     YA(i,2)=0;
    end
end
YA

% % 
% % %Calculating results for XB
% % N= size(XB)
% % N=N(1)
% % for i=1:N
% %     i
% %      mat=[XB(i,1)    XB(i,2)   XB(i,3)
% %           0         1           1
% %           0         1           1]'
% % %                        mat=[XA(i,1)         1           1    %set different machine data
% % %                             0         1           1
% % %                             0         1           1]'
% %      op=[XB(i,4)    XB(i,5)    XB(i,6)]
% %                %  op=[1    1    1];
% %      jobCount=XB(i,10) ; %jobCount=1000 
% %      matCount=XB(i,11);  matCount=1
% %      param1=XB(i,7);
% %      param2=XB(i,8) ;
% %      releaseRate=XB(i,9);
% %      simPerSched=50;
% %     % [Throughput, OperatorEfficiency]= AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)
% %     try
% %       [YB(i,1),YB(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)
% %     catch
% %      YB(i,1)=0;
% %      YB(i,2)=0;
% %     end
% % % % %   %   YA(i,1) --> Throughput
% % % % %    % YA(i,2) --> OperatorEfficiency
% % end
% % 
% % 
% % %Calculating results for XC
% % N= size(XC)
% % N=N(1)
% % for i=1:N
% %     i
% %      mat=[XC(i,1)    XC(i,2)   XC(i,3)
% %           0         1           1
% %           0         1           1]'
% % %                        mat=[XA(i,1)         1           1    %set different machine data
% % %                             0         1           1
% % %                             0         1           1]'
% %      op=[XC(i,4)    XC(i,5)    XC(i,6)]
% %                %  op=[1    1    1];
% %      jobCount=XC(i,10) ; %jobCount=1000 
% %      matCount=XC(i,11);  matCount=1
% %      param1=XC(i,7);
% %      param2=XC(i,8) ;
% %      releaseRate=XC(i,9);
% %      simPerSched=50;
% %     % [Throughput, OperatorEfficiency]= AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)
% %     try
% %       [YC(i,1),YC(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched)
% %     catch
% %      YC(i,1)=0;
% %      YC(i,2)=0;
% %     end
% % % % %   %   YA(i,1) --> Throughput
% % % % %    % YA(i,2) --> OperatorEfficiency
% % end

save('SampleSimulation2')
