n=1000
k = 1:1:n;
k=k';
u=zeros(n,1);
for i=1:n
u(i,1)=fcn(k(i,1))+1;
end

figure
scatter(k,u)


L=u(n,1);
Delta=0.1;
i=n;

while abs(u(i,1)-L)/L < Delta
i=i-1;
if i<=0
break
end

end

n0=i+1;

function y= fcn(x)
y=1/x
end
