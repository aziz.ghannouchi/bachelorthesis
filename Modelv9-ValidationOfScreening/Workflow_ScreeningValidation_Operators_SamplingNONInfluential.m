%% Description
%This is the main workflow for the global sensitivity analysis performed on
%the simulation that modelizes a production plant with Laser Powder Bed
%Fusion

%This script was written by Aziz Ghannouchi in the context of the bachelor
%thesis "Sensitivity analysis of a simulation model to determine the key   
%performance indicators for laser powder bed fusion", under the supervision
%of Tobias Stittgen at the insitute of Digital Additive Production DAP of
%RWTH Aachen University.

logname=['logOutputCalculationBA',datestr(now,'yyyymmddTHHMMSS')];
diary(logname)
diary ON

%% Step 1: set paths

my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir);
addpath(genpath(my_dir));



%% Step 2: setup the model and define input ranges
% Define input distribution and ranges:
%Parameters: Machine availability, Operator presence, Work Distribution,
%Release rate

disp('Factor Fixing validation of the NUMBER OF OPERATORS')
disp('Sampling NON INFLUENTIAL inputs (Number of operators)')
disp('INFLUENTIAL inputs are fixed to the MEANS')

load('XMatrixSamplingALL2')

%% Ranges of the parameters
distr_par={ 10,              4,                  4,                      6,              6,              6,     [1 144]    ,   [0.1 25]   	,   [0.1 12]}

% disp('DistParam1:')
% cell2mat(distr_par(7))
% disp('DistParam2:')
% cell2mat(distr_par(8))
% disp('Release Rate:')
% cell2mat(distr_par(9))



%% Number of parameters, given as the length of the vector
M=length(XNames);

%% Number of generated samples
%Note that this is not the number of simulations performed
%Because of the "Resampling", the total number of simulations is equal to
%N*0.5*(M+2)
N=2000;


%% Modify the samples
% X=[X(:,1) , ones(N,5) , X(:,7), X(:,8), X(:,9)];

Machinesmean=round(mean(X(:,1)));
DistParam1mean=mean(X(:,7));
DistParam2mean=mean(X(:,8));
ReleaseRatemean=mean(X(:,9));

% Mount time and unmount time fixed to 2
X=[Machinesmean*ones(N,1), 2*ones(N,2), X(:,4), X(:,5), X(:,6), DistParam1mean*ones(N,1),DistParam2mean*ones(N,1),ReleaseRatemean*ones(N,1)];

%% Calculate the results for XA 
Y=zeros(N,2);
tic
disp('Calculating Y:')
parfor i=1:N
%Assign values from the generated matrix to the variables of the simulation
      mat=[X(i,1)  X(i,2)    X(i,3)
           0        1          1 
           0        1          1]'       

     op=[X(i,4)    X(i,5)    X(i,6)];
               %  op=[1    1    1];
     matCount=1;
     param1=X(i,7);
     param2=X(i,8) ;
     releaseRate=X(i,9);
          jobCount=round(7000*2*releaseRate) ; %jobCount=1000 
     simPerSched=30;
 
       %     Assign the outputs to the results matrix
       %     YA(i,1) --> Throughput
       %     YA(i,2) --> OperatorEfficiency
   try
% % % % % %       [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
%        [YA(i,1),YA(i,2)]=AzizTest1CalculateOutputs([XA(i,1)  XA(i,2)    XA(i,3); 0 1 1;0 1 1],[XA(i,4)    XA(i,5)    XA(i,6)],round(7000*2*XA(i,9)),1,XA(i,7),XA(i,8),XA(i,9),30);
       [a,b]=AzizTest1CalculateOutputs(mat,op,jobCount,matCount,param1,param2,releaseRate,simPerSched);
Y(i,:)=[a,b];

%YA(i,1)=YA(i,1)/XA(i,1);
  catch
   Y(i,:)=[0,0];
   end
      disp(['YA(',num2str(i),') = '] )
      disp(Y(i,:))

end
Y(:,1)=Y(:,1)./X(:,1);
Y
toc


%% Save workflow for further use
% saves the workspace for further use: to perform the next steps of the
% sensitivity analysis
FileName=['Workspace',datestr(now,'yyyymmddTHHMMSS'),'SamplingNONInfluential']
save(FileName)
diary OFF
