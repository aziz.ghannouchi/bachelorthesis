function schedules = createSchedules(jobCount,matCount,distParam1,distParam2)
%define job / schedule parameters
% longestJob = str2num(longestJob);
jobCount = str2num(jobCount);
matCount = str2num(matCount)

%parametrize distribution
param1=str2num(distParam1);
param2=str2num(distParam2);
param3=1;
distType = 'Gamma';

clear schedule prodKPIs WIP;
dist = struct('Type',distType,'Param1',0,'Param2',0);
stats = struct('avg',0,'std',0,'var',0);
schedules = struct('schedule',cell(1,1),'dist',dist,'stats',stats);
paramCombinations = uint16(length(param1) * length(param2) * length(param3));

i = 1;
for eachParam1 = param1
    for eachParam2 = param2
        for eachParam3 = param3
            %pre-calculate avg depending on param1 and param2 of Gamma dist
            avg = eachParam1 * eachParam2;
            if avg < 120
                dist = makedist(distType,eachParam1,eachParam2);
                i 
                schedules(:,i).schedule = createSchedule(jobCount,matCount,dist);
                schedules(i).dist.Type = distType;
                schedules(i).dist.Param1 = eachParam1;
                schedules(i).dist.Param2 = eachParam2;
                %             schedules(i).dist.Param3 = eachParam3;
                schedules(i).stats.avg = mean(schedules(1,i).schedule(:,1));
                schedules(i).stats.std = std(schedules(1,i).schedule(:,1));
                schedules(i).stats.var = schedules(i).stats.std / schedules(i).stats.avg;
                i = i+1;
            end
        end
    end
end
end
