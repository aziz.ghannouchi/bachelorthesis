function X2=LHS_DistParam(X,M,N)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define input variable
% x1=1:0.1:144;
x1=linspace(0.1,25,10*length(X));
x1=x1';
%% Define functions
f1=ones(length(x1),1)*144;   %Constant
f2=120./x1;                 %second function
f3=3600./(x1.^2);           %Third function
% % % % f3=60./sqrt(x1);
f4=zeros(length(x1),1);     %Actual function used

for i=1:length(x1)
%     f4(i)=min([f1(i),f2(i)]);    
f4(i)=min([f1(i),f2(i),f3(i)]);      %Calculate the values of f4
end
    areaf=0;
for i=2:length(x1)
   areaf=areaf+abs((f4(i)+f4(i-1))*0.5*(x1(i)-x1(i-1)));    %Calculate the area of f4
end
    
%% Points for representation of the axes
% xx=[0 150];         
% oo=[0 0];
% yy=[150 0];

%% Plot the functions
figure
%plot(x1,f1)
hold on
%plot(x1,f2)
%plot(x1,f3)
plot(x1,f4)
% plot(xx,oo)
% plot(oo,xx)
xlim([0,30])
ylim([0 150])



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Second function
% x2=0.1:0.05:25;
x2=linspace(1,144,10*length(X));
x2=x2';
g1=25*ones(length(x2),1);
g2=120./x2;
g3=60./sqrt(x2);

g4=zeros(length(x2),1);

for i=1:length(x2)
        g4(i)=min([g1(i),g2(i),g3(i)]);
% g4(i)=min([g1(i),g2(i)]);

end
    areag=0;
for i=2:length(x2)
   areag=areag+abs((g4(i)+g4(i-1))*0.5*(x2(i)-x2(i-1))) ;
end

figure
% plot(x2,g1)
hold on
% plot(x2,g2)
% plot(x2,g3)
plot(x2,g4)
% plot(xx,oo)
% plot(oo,xx)
xlim([0,150])
ylim([0 30])



f4new=f4/areaf;
g4new=g4/areag;
% x1new=(x1-min(x1)*ones(length(x1),1))/max(x1);
x1new=(x1-min(x1)*ones(length(x1),1))/(max(x1)-min(x1));

x2new=(x2-min(x2)*ones(length(x2),1))/(max(x2)-min(x2));


cdff4=zeros(length(x1),1);
for i=2:length(x1)
cdff4(i)=cdff4(i-1)+f4(i);
end
cdff4=cdff4/max(cdff4);

cdfg4=zeros(length(x2),1);
for i=2:length(x2)
cdfg4(i)=cdfg4(i-1)+g4(i);
end
cdfg4=cdfg4/max(cdfg4);




% 
% figure
% plot(x1new,f4new)

% % figure
% % plot(x1,cdff4)
% % 
% % % figure
% % % plot(x2new,g4new)
% % 
% % figure
% % plot(x2,cdfg4)
% % 

% figure
% plot(cdff4,x1)

% X=lhcube(length(x1),2);

X2=zeros(length(X),2);
% xxx=0.01:0.01:1;
% xxx=linspace(0.01,1,length(x1))
for i=1:length(X)
    z=find(abs(X(i,1)-cdff4)<= 0.005);
    z=min(z);
X2(i,1)=x1(z);
end
%  X2(:,1)=X2(:,1)*139+1;

% xxx=linspace(0.01,1,length(x2))
for i=1:length(X)
    z=find(abs(X(i,2)-cdfg4)<= 0.005);
    z=min(z);
X2(i,2)=x2(z);
end
% X2(:,2)=X2(:,2)*24.9+0.1;




% % 
% %     par1=0.1;
% %     par2=25;
% %     lbound=par1*ones(length(x1),1)
% %     ubound=ones(length(x1),1);
% %     for j=1:length(x1)
% % %        ubound(j)=ubound(j)*min([par2,max([120/X2(j,1),3600/(X2(j,1))^2])]) ;
% % ubound(j)=ubound(j)*min([par2,120/X2(j,1),3600/(X2(j,1))^2]) ;
% %     end



% %     X2(:,2) = feval(['unif' 'inv'],X(:,2),lbound(:),ubound(:));


figure
scatter(X2(:,2),X2(:,1),'.')
hold on
plot(x2,g4,'LineWidth',2.5)

b=X2(:,1);
a=X2(:,2);

mu=a.*b;
sigma=sqrt(a).*b;

mean2=0:0.5:120;
 std2=0:0.5:60;
 a2= (mean2.^2) ./ (std2.^2)';
 b2= (std2.^2)' ./ (mean2);

figure
scatter(mu,sigma,'.')
hold on
xlim([0,120])
ylim([0,60])
contour(mean2,std2,b2,0.1:5:25.1,'blue','ShowText','on');
contour(mean2,std2,a2,[1,10,20,50,144],'black','ShowText','on');
xlabel('mean')
ylabel('STD')

figure
histogram(a)
xlabel('a')

figure
histogram(b)
xlabel('b')

z=0
for i=1:length(X2)
    if (mu(i)>120 | sigma(i)>60)
        z=z+1;
    end
end

% X(:,1) = feval([name 'inv'],X(:,i),pars(1),pars(2));
% 
% f1=ones(length(x1),1)*25;
% f2=120./x1;
% f3=3600./(x1.^2);
% f4=zeros(length(x1),1);
% 
% for i=1:length(x1)
%     f4(i)=min([f1(i),max([f2(i),f3(i)])]);
% end
% 
% syms f(x)
% syms f11(x)
% f11(x)=120/x
% syms f22(x)
% f22(x)=3600/x^2
% syms f33(x)
% f33(x)=max([f11(x),f22(x)])
% f(x) = min([25,max([120/x,3600/(x^2)])])
% 



