%% Define in and outputs
X=[XA;XB];
Y=[YA;YB];

%% Define variables to be investigated
TP=Y(:,1);
releaseRate=X(:,9);
a=X(:,7);
b=X(:,8);

mu=a.*b;
sigma=sqrt(a).*b;
var=sigma./mu;


figure
scatter(releaseRate,TP,'.')
xlabel('Release Rate')
ylabel('Throughput')

figure
scatter(mu,releaseRate,[],TP,'.')
colormap(jet)
xlim([0 200])
% caxis([20 50])
xlabel('Work Content mean')
ylabel('Release Rate')
c = colorbar;
c.Label.String = 'Throughput';

figure
scatter(releaseRate,TP,[],var,'.')
colormap(jet)
xlabel('Release Rate')
ylabel('Throughput')
c = colorbar;
c.Label.String = 'Variation coefficient';

%% Divide TP into intervals
T=struct
T(1).z=find(TP<0.1);
for i=2:10
   T(i).z=find(TP>0.1*(i-1) & TP<(0.1*i)); 
end

%% Divide the WC mean into intervals
% Tvar=struct
% Tvar(1).ind=find(mu<10);
% for i=2:20
%    Tvar(i).ind=find(mu>10*(i-1) & mu<(10*i));    
% end

%% Divide the standard deviation of the WC into intervals
Tvar=struct
Tvar(1).ind=find(sigma<3);
for i=2:20
   Tvar(i).ind=find(sigma>3*(i-1) & sigma<(3*i));    
end



%% Divide the 'a' into intervals
Ta=struct;
Ta(1).z=find(a<2);
for i=2:20
   Ta(i).z=find(a>2*(i-1) & a<(2*i));    
end

%% Divide 'b' into intervals
Tb=struct;
Tb(1).z=find(b<0.2);
for i=2:20
   Tb(i).z=find(b>0.2*(i-1) & b<(0.2*i));    
end

%% Divide 'var' into intervals
Tvar=struct;
Tvar(1).z=find(var<0.05);
for i=2:20
   Tvar(i).ind=find(var>0.05*(i-1) & var<(0.05*i));    
end


%% Divide 'a' and 'b' into intervals
Tab=struct;
Tab(1,1).z=find(b<0.2 & a<2);
for i=2:20
    for j=2:20
   Tab(i,j).z=find(b>0.2*(i-1) & b<0.2*i & a>2*(j-1) & a<j*i);    
    end
end
%%
j=2

figure
for i=1:20
subplot(4,5,i)
scatter(releaseRate(Tvar(i).ind),TP(Tvar(i).ind),'.')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

figure
for i=1:10
subplot(2,5,i)
scatter(mu(T(i).z),releaseRate(T(i).z),[],TP(T(i).z),'.')
xlim([0,200])
caxis([0 1])
xlabel('mu')
ylabel('release rate')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('TP as a function of release rate and mu')
c = colorbar;
c.Label.String = 'Throughput';

figure
hold on
xlim([0 200])
for i=1:10
    plot(mu(T(i).z),releaseRate(T(i).z),'.')
end
xlabel('Mean work content')
ylabel('Production order release rate')
c = colorbar;
c.Label.String = 'Throughput';
title('TP as a function of the release rate and mu')

figure
scatter(releaseRate,TP,[],mu,'.')
caxis([0 200])
xlabel('Release Rate')
ylabel('Throughput')
% colorbar('name','mean of WC')
colormap(jet)
c = colorbar;
c.Label.String = 'Mean Work content';


% figure
% for i=6;
%     releaseratetmp=releaseRate(Tvar(i).ind);
%     TPtmp=TP(Tvar(i).ind);
% scatter(releaseRate(Tvar(i).xtmp),TP(Tvar(i).z),'.')
% xlabel('Release Rate')
% ylabel('Throughput')
% hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
% title(hello)
% end

syms t;
assume(t>0);
assume(t<1)
x1=1;
y1=1;
c1=0.25;
alfa1=10;

x=x1*(1-(1-t^c1)^(1/c1))+alfa1*x1*t;
y=y1*(1-(1-t^c1)^(1/c1));

figure
fplot(x,y)  

figure
hold on
for x1=0.05:round(max(releaseRate))
    
x=x1*(1-(1-t^c1)^(1/c1))+alfa1*x1*t;
y=y1*(1-(1-t^c1)^(1/c1));
fplot(x,y)  
xlim([0 12])

end

%% Calculate fits for TP as a function of Release Rate
% Tvar contains:
% - the indices for the division according to intervals
% - xtmp the indices of the points
% - The function of the fit
% - The function of the derivative of the fit
syms x

% Tvar(1).xtmp=1;
% Tvar(1).df=1;
% Tvar(1).f2=1;
for i=2:20
    ft= fittype('a*(1-exp(b*x))');
   Tvar(i).f=fit(releaseRate(Tvar(i).ind),TP(Tvar(i).ind),ft,'Start',[1 -2]) ;
   Tvar(i).xtmp=linspace(0,12,length(releaseRate(Tvar(i).ind)));
   Tvar(i).xtmp2=linspace(0,12,10*length(releaseRate(Tvar(i).ind)));
   
   Tvar(i).df=differentiate(Tvar(i).f,Tvar(i).xtmp2);
   Tvar(i).f2=Tvar(i).f(Tvar(i).xtmp2);
   Tvar(i).z=min(find(abs(Tvar(i).f2-ones(10*length(releaseRate(Tvar(i).ind)),1)*0.6)<0.02));
  if isempty(Tvar(i).z)
         Tvar(i).z=min(find(abs(Tvar(i).f2-ones(10*length(releaseRate(Tvar(i).ind)),1)*0.6)<0.04));
  end
  if isempty(Tvar(i).z)
         Tvar(i).z=min(find(abs(Tvar(i).f2-ones(10*length(releaseRate(Tvar(i).ind)),1)*0.6)<0.06));
  end
   Tvar(i).dfz=Tvar(i).df(Tvar(i).z);
   
   Tvar(i).sigma=sqrt(a(Tvar(i).ind)).*b(Tvar(i).ind);
   Tvar(i).var=var(Tvar(i).ind);
end

%% Same procedure with intervals for the standard deviation instead of the mean
syms x;
for i=2:20
    ft= fittype('a*(1-exp(b*x))');
   Tvar(i).f=fit(releaseRate(Tvar(i).ind),TP(Tvar(i).ind),ft,'Start',[1 -2]) ;
   Tvar(i).xtmp=linspace(0,12,length(releaseRate(Tvar(i).ind)));
   Tvar(i).xtmp2=linspace(0,12,10*length(releaseRate(Tvar(i).ind)));
   
   Tvar(i).df=differentiate(Tvar(i).f,Tvar(i).xtmp2);
   Tvar(i).f2=Tvar(i).f(Tvar(i).xtmp2);
   Tvar(i).z=min(find(abs(Tvar(i).f2-ones(10*length(releaseRate(Tvar(i).ind)),1)*0.6)<0.02));
  if isempty(Tvar(i).z)
         Tvar(i).z=min(find(abs(Tvar(i).f2-ones(10*length(releaseRate(Tvar(i).ind)),1)*0.6)<0.04));
  end
  if isempty(Tvar(i).z)
         Tvar(i).z=min(find(abs(Tvar(i).f2-ones(10*length(releaseRate(Tvar(i).ind)),1)*0.6)<0.06));
  end
   Tvar(i).dfz=Tvar(i).df(Tvar(i).z);
   
   Tvar(i).sigma=sqrt(a(Tvar(i).ind)).*b(Tvar(i).ind);
   Tvar(i).var=var(Tvar(i).ind);
end




%% Plot the fits with the scatter plots
figure
for i=2:20
subplot(4,5,i)
scatter(releaseRate(Tvar(i).ind),TP(Tvar(i).ind),'.')
hold on
plot(Tvar(i).f,'k')
% plot(Tvar(i).f,'k',releaseRate(Tvar(i).z),TP(Tvar(i).z))
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

%% Plot the fits ONLY
figure
for i=2:20
subplot(4,5,i)
% plot(Tvar(i).f,xx,'k')
xlim([0 12])
% plot(Tvar(i).f,'k',releaseRate(Tvar(i).z),TP(Tvar(i).z),'w')
plot(Tvar(i).f)
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')


%% Plot the fits with their derivatives
figure
for i=2:20
subplot(4,5,i)
% plot(Tvar(i).f,xx,'k')
xlim([0 12])
% plot(Tvar(i).f,'k',releaseRate(Tvar(i).z),TP(Tvar(i).z),'w')
plot(Tvar(i).f)
hold on
plot(Tvar(i).xtmp2,Tvar(i).df)
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')


%% Plot the fits with their derivatives
figure
for i=2:20
subplot(4,5,i)
% plot(Tvar(i).f,xx,'k')
xlim([0 12])
% plot(Tvar(i).f,'k',releaseRate(Tvar(i).z),TP(Tvar(i).z),'w')
plot(Tvar(i).f)
hold on
plot(Tvar(i).xtmp2,Tvar(i).df)
if ~isempty(Tvar(i).z)
    scatter([Tvar(i).xtmp2(Tvar(i).z),Tvar(i).xtmp2(Tvar(i).z)],[Tvar(i).df(Tvar(i).z),Tvar(i).f2(Tvar(i).z)])
end 
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

% Tz1 contains the derivative of the TP at TP=0.6
Tz(1)=0;
i=1;
for i=2:20
    if ~isempty(Tvar(i).z)
Tz1(i)=Tvar(i).df(Tvar(i).z);
   end
end
   % mus contains the WC
mus=linspace(1,20,length(Tz1));

figure
plot(mus*10,Tz1)
xlabel('Mean work content [h]')
ylabel('Derivative of the TP for the point where TP=60%')
title('Relationship between the derivative of the throughput and the mean WC')



%% Use analogy with Condensator
% Find Tau
for i=2:20
slope=Tvar(i).df(1);

    Tvar(i).tau=Tvar(i).xtmp2(min(find(abs(Tvar(i).f.a-slope*Tvar(i).xtmp2)<0.01)));
if isempty(Tvar(i).tau)
     Tvar(i).tau=Tvar(i).xtmp2(min(find(abs(Tvar(i).f.a-slope*Tvar(i).xtmp2)<0.02))); 
end
if isempty(Tvar(i).tau)
     Tvar(i).tau=Tvar(i).xtmp2(min(find(abs(Tvar(i).f.a-slope*Tvar(i).xtmp2)<0.03))); 
end
end


figure
for i=2:20
slope=Tvar(i).df(1);
    subplot(4,5,i)
% plot(Tvar(i).f,xx,'k')
xlim([0 12])
% plot(Tvar(i).f,'k',releaseRate(Tvar(i).z),TP(Tvar(i).z),'w')
plot(Tvar(i).f)
hold on
plot(Tvar(i).xtmp2,Tvar(i).f.a*ones(length(Tvar(i).xtmp2),1))
plot(Tvar(i).xtmp2,slope*Tvar(i).xtmp2)

scatter([Tvar(i).tau Tvar(i).tau],[Tvar(i).f(Tvar(i).tau) Tvar(i).f.a])

xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

tau=0, as=0; sigmas=0;
for i=2:20
   tau(i)=-1/(Tvar(i).f.b);
   as(i)=Tvar(i).f.a
   sigmas(i)=sqrt(a(Tvar(i).ind)).*b(Tvar(i).ind);
end


figure
plot(mus*10,tau)
xlabel('Mean Work Content [h]')
ylabel('Tau, Release rate to reach 63% of the maximum TP')
hold on
plot(mus*10,as)
legend('Tau','Max TP')


%% Plot the fits with the scatter plots
figure
for i=2:20
subplot(4,5,i)
scatter(releaseRate(Tvar(i).ind),TP(Tvar(i).ind),[],sqrt(a(Tvar(i).ind)).*b(Tvar(i).ind),'.')
caxis([0 70])
colormap(jet)
colorbar
hold on
plot(Tvar(i).f,'k')
% plot(Tvar(i).f,'k',releaseRate(Tvar(i).z),TP(Tvar(i).z))
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['var between ', num2str(0.05*(i-1)),' and ', num2str(0.05*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')