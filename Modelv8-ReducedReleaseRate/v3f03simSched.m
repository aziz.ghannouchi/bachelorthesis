function [ResultWIP,ResultprodKPIs] = v3f03simSched(operators, machines,Schedules, simPerSched, releaseRate)

tmpOperators = operators;
tmpMachines = machines;

tmpSchedules = Schedules; 
schedCount = length(Schedules);

%tmpOutWIP       =   zeros(schedCount,simPerSched);
%tmpOutKPIs      =   zeros(schedCount,simPerSched);

%parfor i=1:schedCount
for i=1:schedCount

    %disp(['sim sched: ',num2str(i)]);
    for j=1:simPerSched
        [WIP, prodKPIs] = v3simulateProduction(tmpSchedules(i).schedule, tmpOperators, tmpMachines, releaseRate);
        
        tmpOutWIP(i,j) = WIP;
        tmpOutKPIs(i,j) = prodKPIs;
        
    end
    
end

%fprintf('Executed simulation runs: %d\n',schedCount*simPerSched) 

%count of feasible results
numResults = length(tmpOutWIP);



%assign values to UserData
%for i = 1:numResults           ASK TOBI
 for i = 1:schedCount
   % schedPanel.UserData(i).WIP = tmpOutWIP(i,:);
   % schedPanel.UserData(i).prodKPIs = tmpOutKPIs(i,:);
    
   ResultWIP(i,:) = tmpOutWIP(i,:);
   ResultprodKPIs(i,:) = tmpOutKPIs(i,:);
  
  
  
    
    
end

%update schedule list with number of schedules
%scheduleList.Items = string(1:numResults);
%scheduleList.Value = {};

%update simList dropdown with number of simulations
%simList.Items = string(1:numResults);

end