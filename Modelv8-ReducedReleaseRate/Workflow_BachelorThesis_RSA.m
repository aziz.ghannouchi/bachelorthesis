%% Description
% This script is the workflow for the regional sensitivity analysis of a
% simulation model to determine the key performance indicators for laser
% powder bed fusion.

% Starting for a set of generated simulation outputs, this script
% calculates the sensitivity indices of a Monte Carlo Filtering (aka. Factor Mapping)

%This script was written by Francesca Pianosi and Fanny Sarrazin
% Adapted by Aziz Ghannouchi in the context of the bachelor
%thesis "Sensitivity analysis of a simulation model to determine the key   
%performance indicators for laser powder bed fusion", under the supervision
%of Tobias Stittgen at the insitute of Digital Additive Production DAP of
%RWTH Aachen University.

% University of Bristol 2014
% mail to: francesca.pianosi@bristol.ac.uk
%
% METHODS
%
% This script provides an example of application of Regional Sensitivity
% Analysis (RSA). RSA is applied according to the original formulation
% where the input samples are split into two datasets depending on whether
% the corresponding output satisfies a threshold condition 
% >> matlab functions: RSA_indices_thres, RSA_plot_thres, RSA_convergence_thres
% (see help of 'RSA_indices_thres' for more details and references)
%
% INDEX
%
% Steps:
% 1. Add paths to required directories
% 2. Load Workspace containing the input and ouput samples generated with
% the simulation
% 3. Perform RSA with thresholds 



%% Step 1 (add paths)
my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir)
addpath(genpath(my_dir))

% % %% Step 2 (setup the Hymod model)
% % 
% % % Load data:
% % load -ascii LeafCatch.txt
% % rain = LeafCatch(1:365,1)   ;
% % evap = LeafCatch(1:365,2)   ;
% % flow = LeafCatch(1:365,3)   ;
% % 
% % % Define inputs:
% % DistrFun  = 'unif'  ; % Parameter distribution
% % DistrPar  = { [ 0 400 ]; [ 0 2 ]; [ 0 1 ]; [ 0 0.1 ] ; [ 0.1 1 ] } ; % Parameter ranges (from literature)
% % x_labels = {'Sm','beta','alfa','Rs','Rf'} ;
% % 
% % % Define output:
% % myfun = 'hymod_MulObj' ;
% % 
% % %% Step 3 (sample inputs space)
% % SampStrategy = 'lhs' ; % Latin Hypercube
% % N = 3000 ; % Number of samples
% % M = length(DistrPar) ; % Number of inputs
% % X = AAT_sampling(SampStrategy,M,DistrFun,DistrPar,N);
% % 
% % %% Step 4 (run the model) 
% % Y = model_evaluation(myfun,X,rain,evap,flow) ;

%% Step 5a (Regional Sensitivity Analysis with threshold)
X_Labels={    'Number of Machines',    'Mount Time',    'Unmount Time',    'Operator0',   'Operator8',   'Operator16',  'Shape Param of WC',  'Scale Param of WC',  'Release Rate'};

X=[XA;XB];
Y=[YA;YB];

% Visualize input/output samples (this may help finding a reasonable value
% for the output threshold):
figure; scatter_plots(X,Y(:,1),[],'Throughput',X_Labels) ;    
figure; scatter_plots(X,Y(:,2),[],'OperatorEfficiency',X_Labels) ;

%figure; scatter_plots(X,Y(:,1),[],'rmse',x_labels) ;
%figure; scatter_plots(X,Y(:,2),[],'bias',x_labels) ;

% Set output threshold:
TP_thres = 0.6   ; %  threshold for the first obj. fun.
OE_thres = 0 ; % behavioural threshold for the second obj. fun.

% RSA (find behavioural parameterizations):
threshold = [ TP_thres OE_thres ] ;
[mvd,idxb] = RSA_indices_thres(X,Y,threshold) ;
%The region of interest is the higher region of the output, therefore invert the idxb
%idxb=~idxb;
% threshold2= [0.6 0];
% threshold3=[0.9 1];
% [mvd2,idxb2]=RSA_indices_thres2(X,Y,threshold2,threshold3) ;
% idxbnew=idxb2 & idxb3;

%% Highlight the behavioural parameterizations in the scatter plots:
figure; scatter_plots(X,Y(:,1),[],'Throughput',X_Labels,idxb) ;
figure; scatter_plots(X,Y(:,2),[],'OperatorEfficiency',X_Labels,idxb) ;

%% Highlight the behavioural parameterizations (with middle TP) in the scatter plots:
% figure; scatter_plots(X,Y(:,1),[],'Throughput',X_Labels,idxb2) ;
% figure; scatter_plots(X,Y(:,2),[],'OperatorEfficiency',X_Labels,idxb2) ;



% Plot parameter CDFs:
RSA_plot_thres(X,idxb,[],X_Labels,{'Behavioral Inputs','Non-behavioral Inputs'}); % add legend

% Check the ranges of behavioural parameterizations by
% Parallel coordinate plot:
% parcoor(X,X_Labels,[],idxb)

% Plot the sensitivity indices (maximum vertical distance between
% parameters CDFs):
figure; boxplot1(mvd,X_Labels,'mvd')
xtickangle(45)

% Plot the sensitivity indices (maximum vertical distance between
% parameters CDFs):
% figure; boxplot1(mvd2,X_Labels,'mvd 60-90%')
% xtickangle(45)

%% Assess robustness by bootstrapping:
Nboot = 100;
[mvd,idxb,mvd_lb,mvd_ub] = RSA_indices_thres(X,Y,threshold,[],Nboot);
% Plot results:
boxplot1(mvd,X_Labels,'mvd',mvd_lb,mvd_ub)

%% Repeat computations using an increasing number of samples so to assess
% convergence:
N=length(X)
NN = [ N/5:N/5:N ] ;
mvd = RSA_convergence_thres(X,Y,NN,threshold) ;
% Plot the sensitivity measures (maximum vertical distance between
% parameters CDFs) as a function of the number of samples:
figure; plot_convergence(mvd,NN,[],[],[],'no of samples','mvd',X_Labels)

% Repeat convergence analysis using bootstrapping to derive confidence
% bounds:
Nboot = 100;
[mvd_mean,mvd_lb,mvd_ub]  = RSA_convergence_thres(X,Y,NN,threshold,[],Nboot) ;
figure
plot_convergence(mvd_mean,NN,mvd_lb,mvd_ub,[],'Number of Model Runs','MVD',X_Labels)

%% Regional Sensitivity Analysis with groups

% RSA (find behavioural parameterizations):
[stat,idx,Yk] = RSA_indices_groups(X,Y(:,1)) ;

% Plot parameter CDFs:
RSA_plot_groups(X,idx,Yk) ;
% Customize labels and legend:
RSA_plot_groups(X,idx,Yk,[],X_Labels,'Throughput'); % add legend
