

%% Description
% This script is part of the workflow for the Global Sensitivity analysis
% of a simulation model to determine the key performance indicators for
% laser powder bed fusion.

% Starting for a set of generated simulation outputs, this script
% calculates the sensitivity indices of a Variance Based Sensitivity
% analysis

%This script was written by Aziz Ghannouchi in the context of the bachelor
%thesis "Sensitivity analysis of a simulation model to determine the key   
%performance indicators for laser powder bed fusion", under the supervision
%of Tobias Stittgen at the insitute of Digital Additive Production DAP of
%RWTH Aachen University.

%% Step 1 (add paths)
my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir)
addpath(genpath(my_dir))

%% Calculate the sensitivity indices
% Nomenclature:
%%%Si --> Main effect
%%%ST --> Total effect
%%% TP --> Throughput
%%% OE --> Operator Efficiency

[ SiTP, STiTP, Si_sdTP, STi_sdTP, Si_lbTP, STi_lbTP, Si_ubTP, STi_ubTP ] = vbsa_indices_ModifiedComputeIndices(YA(:,1),YB(:,1),YC(:,1));
[ SiOE, STiOE,  Si_sdOE, STi_sdOE, Si_lbOE, STi_lbOE, Si_ubOE, STi_ubOE] = vbsa_indices_ModifiedComputeIndices(YA(:,2),YB(:,2),YC(:,2));

% function [ Si, STi, Si_sd, STi_sd, Si_lb, STi_lb, Si_ub, STi_ub, Si_all, STi_all ] = vbsa_indices_ModifiedComputeIndices(YA,YB,YC,varargin)



%Labels for the plots, each one is a parameter of the simulation
X_Labels={    'MachineA',    'MountingTimeA',    'UnmountingTimeA',    'Operator0',   'Operator8',   'Operator16',  'DistParam1',  'DistParam2',  'ReleaseRate'};

%% Results regarding the Throughput
% plot main and total separately
figure
subplot(121); boxplot1(SiTP,X_Labels,'Main effects')
xtickangle(45)
subplot(122); boxplot1(STiTP,X_Labels,'Total effects')
xtickangle(45)
sgtitle ('Sensitivity indices on Throughput')
% figure(figure_number, 'Position', [0 0 13 8]);

figure % plot both in one plot:
title ('Sensitivity indices on Throughput')
boxplot2([SiTP; STiTP],X_Labels)
legend('Main effects','Total effects')% add legend
xtickangle(45)

%% Plot results for the operator efficiency
figure % plot main and Total separately
subplot(121); boxplot1(SiOE,X_Labels,'Main effects')
xtickangle(45)
subplot(122); boxplot1(STiOE,X_Labels,'Total effects')
xtickangle(45)
title ('Sensitivity indices on Operator Efficiency')

figure % plot both in one plot:
boxplot2([SiOE; STiOE],X_Labels)
legend('Main effects','Total effects')% add legend
xtickangle(45)
title ('Sensitivity indices on Operator Efficiency')


%% Compute confidence bounds:
Nboot = 500 ;
[ SiTP, STiTP, Si_sdTP, STi_sdTP, Si_lbTP, STi_lbTP, Si_ubTP, STi_ubTP ] = vbsa_indices_ModifiedComputeIndices(YA(:,1),YB(:,1),YC(:,1),Nboot);
[ SiOE, STiOE, Si_sdOE, STi_sdOE, Si_lbOE, STi_lbOE, Si_ubOE, STi_ubOE ] = vbsa_indices_ModifiedComputeIndices(YA(:,2),YB(:,2),YC(:,2),Nboot);

% Plot TP:
figure % plot Main and Total separately
subplot(121); boxplot1(SiTP,X_Labels,'Main effects',Si_lbTP,Si_ubTP)
xtickangle(45)
subplot(122); boxplot1(STiTP,X_Labels,'Total effects',STi_lbTP,STi_ubTP)
xtickangle(45)
title ('Sensitivity indices on Throughput')

figure % plot both in one plot:
boxplot2([SiTP; STiTP],X_Labels,[ Si_lbTP; STi_lbTP ],[ Si_ubTP; STi_ubTP ])
xtickangle(45)
title ('Sensitivity indices on Throughput')
legend('Main effects','Total effects')

% Plot OE:
figure % plot Main and Total separately
subplot(121); boxplot1(SiOE,X_Labels,'Main effects',Si_lbOE,Si_ubOE)
xtickangle(45)
subplot(122); boxplot1(STiOE,X_Labels,'Total effects',STi_lbOE,STi_ubOE)
xtickangle(45)
title ('Sensitivity indices on Operator Efficiency')

figure % plot both in one plot:
boxplot2([SiOE; STiOE],X_Labels,[ Si_lbOE; STi_lbOE ],[ Si_ubOE; STi_ubOE ])
xtickangle(45)
title ('Sensitivity indices on Operator Efficiency')

legend('Main effects','Total effects')


%% Analyze convergence of sensitivity indices:
%NN = [N/10:N/10:N] ;
NA=length(XA)
NN = [NA/10:NA/10:NA] ;

M=9
% Throughput
[ SicTP, STicTP ] = vbsa_convergence([YA(:,1);YB(:,1);YC(:,1)],M,NN);
figure
subplot(121); plot_convergence(SicTP,NN*(M+2),[],[],[],'Model evaluations','Main effect',X_Labels)
xtickangle(45)
subplot(122); plot_convergence(STicTP,NN*(M+2),[],[],[],'Model evaluations','Total effect',X_Labels);
xtickangle(45)
title ('Sensitivity indices on Throughput')

% With confidence bounds:
[ SicTP, STicTP, Si_sdcTP, STi_sdcTP, Si_lbcTP, STi_lbcTP, Si_ubcTP, STi_ubcTP  ] = vbsa_convergence([YA(:,1);YB(:,1);YC(:,1)],M,NN,Nboot);
figure
subplot(221); plot_convergence(SicTP,NN*(M+2),Si_lbcTP,Si_ubcTP,[],'Model evaluations','Main effect',X_Labels)
xtickangle(45)
subplot(222); plot_convergence(STicTP,NN*(M+2),STi_lbcTP,STi_ubcTP,[],'Model evaluations','Total effect',X_Labels);
xtickangle(45)
title ('Sensitivity indices on Throughput')

% Operator Efficiency
[ SicOE, STicOE ] = vbsa_convergence([YA(:,2);YB(:,2);YC(:,2)],M,NN);
figure
subplot(121); plot_convergence(SicOE,NN*(M+2),[],[],[],'Model evaluations','Main effect',X_Labels)
xtickangle(45)
subplot(122); plot_convergence(STicOE,NN*(M+2),[],[],[],'Model evaluations','Total effect',X_Labels);
xtickangle(45)
title ('Sensitivity indices on Operator Efficiency')

% With confidence bounds:
[ SicOE, STicOE, Si_sdcOE, STi_sdcOE, Si_lbcOE, STi_lbcOE, Si_ubcOE, STi_ubcOE  ] = vbsa_convergence([YA(:,2);YB(:,2);YC(:,2)],M,NN,Nboot);
figure
subplot(121); plot_convergence(SicOE,NN*(M+2),Si_lbcOE,Si_ubcOE,[],'Model evaluations','Main effect',X_Labels)
xtickangle(45)
% xticks([NN(2:2:10)*(M+2)])
subplot(122); plot_convergence(STicOE,NN*(M+2),STi_lbcOE,STi_ubcOE,[],'Model evaluations','Total effect',X_Labels);
% xticks([NN(2:2:10)*(M+2)])

xtickangle(45)
title ('Sensitivity indices on Operator Efficiency')



% % figure
% % subplot(121)
% % stackedbar([SiTP; SiOE],X_Labels,'Main effects')
% % % stackedbar([SiTP; SiOE],[],'Main effects',[],X_Labels)
% % subplot(122)
% % stackedbar([STiTP; STiOE],X_Labels,'Total effects')
% % % stackedbar([STiTP; STiOE],X_Labels,'total effects',[],X_Labels)
% % 
% % 

