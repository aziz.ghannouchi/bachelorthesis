
%%%Si --> Main effect
%%%ST --> Total effect
%%% TP --> Throughput
%%% OE --> Operator Efficiency
[ SiTP, STiTP ] = vbsa_indices(YA(:,1),YB(:,1),YC(:,1));
[ SiOE, STiOE ] = vbsa_indices(YA(:,2),YB(:,2),YC(:,2));

% Plot results:
X_Labels={    'MachineA',    'MountingTime',    'UnmountingTime',    'Operator0',   'Operator8',   'Operator16',  'DistParam1',  'DistParam2',  'ReleaseRate',  'jobCount', 'matCount'};

figure % plot main and total separately
subplot(121); boxplot1(SiTP,X_Labels,'main effects')
xtickangle(90)
subplot(122); boxplot1(STiTP,X_Labels,'total effects')
xtickangle(90)
title ('Sensitivity indices on Throughput')

figure % plot both in one plot:
boxplot2([Si; STi],X_Labels)
legend('main effects','total effects')% add legend
xtickangle(90)
title ('Sensitivity indices on Throughput')

figure % plot main and total separately
subplot(121); boxplot1(SiOE,X_Labels,'main effects')
xtickangle(90)
subplot(122); boxplot1(STiOE,X_Labels,'total effects')
xtickangle(90)
title ('Sensitivity indices on Operator Efficiency')

figure % plot both in one plot:
boxplot2([Si; STi],X_Labels)
legend('main effects','total effects')% add legend
xtickangle(90)
title ('Sensitivity indices on Operator Efficiency')


