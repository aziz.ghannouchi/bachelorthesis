mean=1:0.1:3;
std=mean+1

dotx=3;
doty=2;
x1=[mean,dotx]'
y1=[std,doty]'
k = boundary(x1,y1)

figure
plot(mean,std)
xlim([0 5])
ylim([0 5])
hold on
scatter(dotx,doty)
plot(x1(k),y1(k))
