% 
%Load X and Y along with the vector idxb that represents the indices that
%lead to the region of interest

n=4;
col=gray(n+1); % Color for colormap, (n+1) so that the last line is not white
map = colormap(col(1:n,:));% Remove white color
fs = 22; ms = 10 ;



%% Define Xnew and Ynew for the output region of interest
indices=[];
k=0;
for i=1:length(X)
    if idxb(i,1)==1
        k=k+1;
        indices(k,1)=i;
    end
end

Xnew=X(indices,:);
Ynew=Y(indices,:);

%% Plot only the output region of interest
figure; scatter_plots(Xnew,Ynew(:,1),[],'Throughput',X_Labels) ;    
figure; scatter_plots(Xnew,Ynew(:,2),[],'OperatorEfficiency',X_Labels) ;


%% Perform PAWN
n=4;
nboot=100;
%% Calculate the indices
[KS_medianTP,KS_meanTP,KS_maxTP,KS_dummyTP,YYTP,xcTP,NCTP,XXTP] = pawn_indices_givendata(Xnew,Ynew(:,1),n,nboot);
[KS_medianOE,KS_meanOE,KS_maxOE,KS_dummyOE,YYOE,xcOE,NCOE,XXOE] = pawn_indices_givendata(Xnew,Ynew(:,2),n,nboot);

%% Calculate statistics across bootstrap resamples
alfa= 0.05;
Spawn_mTP=mean(KS_meanTP);
KS_sortTP=sort(KS_meanTP);
Spawn_lbTP = KS_sortTP(max(1,round(nboot*alfa/2)),:);
Spawn_ubTP = KS_sortTP(round(nboot*(1-alfa/2)),:);

%% Plot the PAWN sensitivity indices with the condifence intervals
figure
boxplot1(Spawn_mTP,X_Labels,'Sensitivity Index TP (PAWN)',Spawn_lbTP,Spawn_ubTP)
xtickangle(45)


[KSTP,KS_dummyTP,YFTP,FuTP,FcTP,YYTP,xcTP,NCTP,XXTP,YUTP,idx_bootstrapTP,YUdTP] = pawn_ks_givendata(Xnew,Ynew(:,1),n)



%% ^^^^ CDFs and KSs ^^^
for i=1:9
figure; hold on;
% (g): plot CDFs
subplot(1,2,1)
for k=1:n
    plot(YFTP,FcTP{i,k},'Color',map(k,:),'LineWidth',2.5); hold on
%         plot(YFTP,FcTP{i,k},'LineWidth',3.5); hold on

end
plot(YFTP,FuTP,'r','LineWidth',2.5)
set(gca,'FontSize',fs); xlabel('Throughput'); ylabel('cdf'); 
% (h): plot KS
subplot(1,2,2)
for k=1:n
    plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerFaceColor',map(k,:),'MarkerSize',ms); hold on
%     plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerSize',ms); hold on

end
set(gca,'FontSize',fs,'XTick',round(xcTP{i},1),'YLim',[0,1],'XLim',[min(X(:,i)),max(X(:,i))],'XGrid','on')
xlabel(X_Labels{i}); ylabel('KS'); 

title(X_Labels{i})
end



%% ^^^^ CDFs and KSs ^^^ --> for report
for i=1:9
figure; hold on;
% (g): plot CDFs
subplot(1,2,1)
for k=1:n
    plot(YFTP,FcTP{i,k},'Color',map(k,:),'LineWidth',2.5); hold on
%         plot(YFTP,FcTP{i,k},'LineWidth',3.5); hold on

end
plot(YFTP,FuTP,'r','LineWidth',2.5)
set(gca,'FontSize',fs); xlabel('Throughput'); ylabel('CDF');
xlim([0 1])
% (h): plot KS
subplot(1,2,2)
for k=1:n
    plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerFaceColor',map(k,:),'MarkerSize',ms); hold on
%     plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerSize',ms); hold on

end
set(gca,'FontSize',fs,'XTick',round(xcTP{i},1),'YLim',[0,1],'XLim',[min(X(:,i)),max(X(:,i))],'XGrid','on')
xlabel(X_Labels{i}); ylabel('KS'); 

xpos=5;ypos=5;
h = gcf; % Current figure handle
% set(h,'Resize','off');
% set(h,'PaperPositionMode','manual');
set(h,'Units','centimeters');
h.Units='centimeters';

set(h,'PaperPosition',[0 0 9 6]);
set(h,'PaperSize',[13 20]); % IEEE columnwidth = 9cm
set(h,'Position',[xpos ypos 12.5 9]);
% xpos, ypos must be set
% txlabel = text(xpos,ypos,'$$[\mathrm{min}]$$','Interpreter','latex','FontSize',9);
% % Dump colored encapsulated PostScript
% print('-depsc2','-loose', 'signals');
set(gca,'FontSize',11,'FontName','Cambria Math')
set(findall(gcf,'-property','FontSize'),'FontSize',11,'FontName','Cambria Math','FontWeight','Normal')


end
