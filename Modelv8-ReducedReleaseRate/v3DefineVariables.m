%This script defines variables that will be used for the simulation
%These are the parameters that will be later modified for the sensitivity
%analysis

%Production settings
op = [0,1,1];                      %Operators
mat= [1,1,1;
      0,1,1;
      0,1,1]';         %Number of Machines, mounting and Unmounting time

%Schedule Creation
jobCount = 1000;                    %Number of jobs
matCount = 1;                      %Number of Materials
param1= [3.2];                 %Distribution Parameter 1
param2= [3.4];                         %Distribution Parameter 2

releaseRate=0.2;
simPerSched=50


%Create Dist
%dist= createDist(1,2,3); 
