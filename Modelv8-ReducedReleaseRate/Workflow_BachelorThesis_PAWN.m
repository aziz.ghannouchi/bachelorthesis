% This workflow scripts demonstrates the approximation strategy 
% of PAWN indices from generic input-output dataset
% by application to the Ishigami-homma function, 
% as described in Section 3.1 of:
%
% Pianosi, F. and Wagener, T. (2018), Distribution-based sensitivity 
% analysis from a generic input-output sample, Env. Mod. & Soft.
%
% The code works together with the SAFE Toolbox. More information and
% dowload at: www.bris.ac.uk/cabot/resources/safe-toolbox/
%
% This code developed by Francesca Pianosi, University of Bristol
% email: francesca.pianosi@bristol.ac.uk

%%%%%%%%%%%%%%%%%%%%% SET PATH %%%%%%%%%%%%%%%%%%%%%

% Set the path to the SAFE Toolbox directory
% (where you must have also copied the additional functions/data 
% for applying PAWN to generic datasets)

my_dir = pwd ; % use the 'pwd' command if you have already setup the Matlab
% current directory to the SAFE directory. Otherwise, you may define
% 'my_dir' manually by giving the path to the SAFE directory, e.g.:
% my_dir = '/Users/francescapianosi/Documents/safe_R1.0';

% Set current directory to 'my_dir' and add path to sub-folders:
cd(my_dir)
addpath(genpath(my_dir))
n=4
col=gray(n+1); % Color for colormap, (n+1) so that the last line is not white
map = colormap(col(1:n,:));% Remove white color
fs = 22; ms = 10 ; % font size and marker size


 X_Labels={    'MachineA',    'MountingTimeA',    'UnmountingTimeA',    'Operator0',   'Operator8',   'Operator16',  'DistParam1',  'DistParam2',  'ReleaseRate'};

nboot=50;
%% Calculate the indices
[KS_medianTP,KS_meanTP,KS_maxTP,KS_dummyTP,YYTP,xcTP,NCTP,XXTP] = pawn_indices_givendata(X,Y(:,1),n,nboot);
[KS_medianOE,KS_meanOE,KS_maxOE,KS_dummyOE,YYOE,xcOE,NCOE,XXOE] = pawn_indices_givendata(X,Y(:,2),n,nboot);

%% Calculate KS
[KSTP,KS_dummyTP,YFTP,FuTP,FcTP,YYTP,xcTP,NCTP,XXTP,YUTP,idx_bootstrapTP,YUdTP] = pawn_ks_givendata(X,Y(:,1),n)

%% Calculate statistics across bootstrap resamples
alfa= 0.05;
Spawn_m=mean(KS_meanTP);
KS_sortTP=sort(KS_meanTP);
Spawn_lb = KS_sortTP(max(1,round(nboot*alfa/2)),:);
Spawn_ub = KS_sortTP(round(nboot*(1-alfa/2)),:);

%% Plot the PAWN sensitivity indices with the condifence intervals
figure
boxplot1(Spawn_m,X_Labels,'Sensitivity Index (PAWN)',Spawn_lb,Spawn_ub)
xtickangle(45)

for NN=[N/5:N/5:N]
X1=X(1:NN,:);
Y1=Y(1:NN, :);
[KS_medianTP,KS_meanTP,KS_maxTP,KS_dummyTP,YYTP,xcTP,NCTP,XXTP] = pawn_indices_givendata(X1,Y1(:,1),n,nboot);
    [KSTP,KS_dummyTP,YFTP,FuTP,FcTP,YYTP,xcTP,NCTP,XXTP,YUTP,idx_bootstrapTP,YUdTP] = pawn_ks_givendata(X1,Y1(:,1),n)
 Spawn_m(NN/N*5,:)=mean(KS_meanTP);
 Spawn_lb(NN/N*5,:)=min(KS_meanTP);
 Spawn_ub(NN/N*5,:)=max(KS_meanTP);  
end

figure
plot_convergence(Spawn_m,[N/5:N/5:N],Spawn_lb,Spawn_ub,[],'Number of Model Runs','PAWN Sensitivity Indices',X_Labels)
xtickangle(45)



%% ^^^^ CDFs and KSs ^^^
for i=1:9
figure; hold on;
% (g): plot CDFs
subplot(1,2,1)
    for k=1:n
        plot(YFTP,FcTP{i,k},'Color',map(k,:),'LineWidth',2.5); hold on
%         plot(YFTP,FcTP{i,k},'LineWidth',3.5); hold on

    end

plot(YFTP,FuTP,'r','LineWidth',2.5)
set(gca,'FontSize',fs); xlabel('Throughput'); ylabel('CDF'); 
% (h): plot KS
subplot(1,2,2)
for k=1:n
    plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerFaceColor',map(k,:),'MarkerSize',ms); hold on
%     plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerSize',ms); hold on

end
set(gca,'FontSize',fs,'XTick',round(xcTP{i},1),'YLim',[0,1],'XLim',[min(X(:,i)),max(X(:,i))],'XGrid','on')
xlabel(X_Labels{i}); ylabel('KS'); 

title(X_Labels{i})
end







%% ^^^^ CDFs and KSs ^^^ --> just for report
for i=1:9
figure; hold on;
% (g): plot CDFs
subplot(1,2,1)
    for k=1:n
        plot(YFTP,FcTP{i,k},'Color',map(k,:),'LineWidth',2.5); hold on
%         plot(YFTP,FcTP{i,k},'LineWidth',3.5); hold on

    end

plot(YFTP,FuTP,'r','LineWidth',2.5)
set(gca,'FontSize',fs); xlabel('Throughput'); ylabel('CDF'); 
% (h): plot KS
subplot(1,2,2)
for k=1:n
    plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerFaceColor',map(k,:),'MarkerSize',ms); hold on
%     plot(xcTP{i}(k),KSTP(k,i),'ok','MarkerSize',ms); hold on

end
set(gca,'FontSize',fs,'XTick',round(xcTP{i},1),'YLim',[0,1],'XLim',[min(X(:,i)),max(X(:,i))],'XGrid','on')
xlabel(X_Labels{i}); ylabel('KS'); 

xpos=5;ypos=5;
h = gcf; % Current figure handle
% set(h,'Resize','off');
% set(h,'PaperPositionMode','manual');
set(h,'Units','centimeters');
h.Units='centimeters';

set(h,'PaperPosition',[0 0 9 6]);
set(h,'PaperSize',[13 20]); % IEEE columnwidth = 9cm
set(h,'Position',[xpos ypos 12.5 9]);
% xpos, ypos must be set
% txlabel = text(xpos,ypos,'$$[\mathrm{min}]$$','Interpreter','latex','FontSize',9);
% % Dump colored encapsulated PostScript
% print('-depsc2','-loose', 'signals');
set(gca,'FontSize',11,'FontName','Cambria Math')
set(findall(gcf,'-property','FontSize'),'FontSize',11,'FontName','Cambria Math','FontWeight','Normal')

end


