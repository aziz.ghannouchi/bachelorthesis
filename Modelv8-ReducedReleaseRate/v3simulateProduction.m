function [WIP,prodKPIs] = v3simulateProduction(jobs,operators,machines,releaseRate)

WIP = struct('started',cell(1,1),'completed',cell(1,1),'queued',cell(1,1));
jobs = [jobs,zeros(length(jobs(:,1)),3)];

completed = [];
started = [];

%convert release rate of production order from daily to hourly
%release new job for production (data prep finished) at a rate of
%releaseRate (measured in jobs per hour and machine
releaseRate = releaseRate / 24;

%counting variable for indexing started and completed jobs
c = 1;
d = 1;

%start simulation with 0800 shift
shift = 8;
n=1;

%create queue and start with randomly selected job
jobID = jobs(randi(length(jobs(1,:))));
queue(1,:) = jobs(jobID,:);
queue(1,8) = 0;

%calculate throughput time depending on process time and
%material-/machine-specific mount-/unmount-time
matIndex = find(machines(:,3) == jobs(jobID,2),1);
wcJob = jobs(jobID,1) + sum(machines(matIndex,1:2));

%add selected job to queue history
queueHist = [d,0,wcJob];
d = d+1;
%remove selected (queued) job from joblist
jobs(jobID,:) = [];

%define machine availability
availability = 1;

%run production for 3840 hours (160 days)
timeframe=3840*2;

%create evenly distributed random values to simulate machine downtime
%randValue = rand(timeframe, length(jobs));


%simulate production hour by hour
for k = 1:timeframe

    %current working hour of shift
    p = mod(k,8)+1;
    
    %release new job at rate defined above
    if rand(1) < releaseRate
        jobID = jobs(randi(length(jobs(1,:))));
        %add job to queue
        queue = [queue; [jobs(jobID,:),0]];
        
        %calculate throughput time depending on process time and
        %material-/machine-specific mount-/unmount-time
        matIndex = find(machines(:,3) == jobs(jobID,2),1);
        wcJob = jobs(jobID,1) + sum(machines(matIndex,1:2));
        
        %add selected job to queue history
        queueHist = [queueHist; [d, k, wcJob]];
        d=d+1;

        %remove selected (queued) job from joblist
        jobs(jobID,:) = [];
    end
    %in case no job is queued (i.e. waiting for or in production) -> skip
    %to next hour
    if isempty(queue)
        continue
    end
       
    %calculate if 8, 16 or 0 h shift
    if mod(k,8) == 0 && shift < 24
        shift = shift + 8;
    elseif shift == 24
        shift = 0;
    end
    
    %check for available operators
    availableOperators = [];
    for m=1:length(operators(:,1))
        if operators(m,2) == 0 && shift == operators(m,1) && operators(m,3) ~= k
            availableOperators = [availableOperators; m];
        end
    end
    
    %check for available machines
    availableMachines = [];
    n=1;
    for j = 1:length(machines(:,1))
        if machines(j,4) == 0 
            availableMachines = [availableMachines; j];
            n=n+1;
        end
    end
    
    %iterate through queued jobs 
    i=1;
    while (i <= size(queue,1))

        %take machine unavailability into account
        %if randValue(k,i) < (1-availability)
        %    continue
        %end
        
        %check for duration and material type of current job
        jobMaterial = queue(i,2);
        
        %check if job material fits material capabilities of available
        %machines
        machineAvailable = ismember(jobMaterial,machines(availableMachines,3));
        
        %check if operator is available
        operatorAvailable = ~isempty(availableOperators);
        if operatorAvailable
            selectedOperator = availableOperators(1);
        end
        
        %start job
        if queue(i,1) == queue(i,3) && queue(i,4) == 0 && operatorAvailable && machineAvailable
            %select 1st machine from list of available machines
            selectedMachine = availableMachines(1,1);
                    
            %operator may only start job if his shift lasts longer than
            %required mounting time
            if 8-p >= machines(selectedMachine,1)
                
                %assign operator to selected machine and queued job
                operators(selectedOperator,2) = selectedMachine;
                queue(i,5) = selectedOperator;
                
                %remove operator from list of available operators
                availableOperators(1) = [];
                
                %assign selected machine to job i in queue
                queue(i,4) = selectedMachine;
                
                %remove selected machine from list of available
                %machines
                availableMachines(1,:) = [];
                     
                %read machine-specific mount- and unmount-time from machines list
                queue(i,6) = machines(selectedMachine,1);
                queue(i,7) = machines(selectedMachine,2);
                
                %block selected machine after start of job
                machines(selectedMachine,4) = 1;
                
                %add mount- and unmount-time to actual job duration to
                %determine throughput time
                throughputTime = queue(i,1) + machines(selectedMachine,1) + machines(selectedMachine,2);
                
                started = [started; [c throughputTime k]];
                queue(i,8) = c;
                c=c+1;
                clear throughputTime;
            end
        end
        
        %mounting procedure ongoing
        if queue(i,6) > 0
            queue(i,6) = queue(i,6) - 1;

            %keep operator blocked during mounting procedure
            operators(selectedOperator,3) = k;
            %release operator (only for operator's currently assigned
            %machine) if mounting procedure is finished
            if queue(i,6) == 0
                operators(selectedOperator,2) = 0;
            end
            
        %job running & mounting finished
        elseif queue(i,3) > 0 && queue(i,4) > 0
            queue(i,6) = -1;
            queue(i,3) = queue(i,3) - 1;      
            
        %job completed & unmount ongoing
        elseif queue(i,3) == 0 && queue(i,7) > 0 && operatorAvailable && 8-p >= jobs(i,7) 
            queue(i,7) = queue(i,7) - 1;
            operators(selectedOperator,3) = k;
            availableOperators(1) = [];
            if queue(i,7) == 0
                queue(i,3) = -1;
            end          

        %unmount completed job
        elseif queue(i,7) == 0 && queue(i,3) == -1 && queue(i,4) > 0
            machines(queue(i,4),4) = 0;
            queue(i,7) = -1;
            %calculate throughput time including mount and unmount
            throughputTime = queue(i,1) + machines(queue(i,4),1) + machines(queue(i,4),2);
            
            %save jobID, throughput-time, finish period and ID of used machine
            completed = [completed; [queue(i,8) throughputTime k queue(i,4)]];
            
            %remove completed job from queue
            queue(i,:) = [];
            clear throughputTime;
        end
     i=i+1;   
    end
   
end

WIP.started = started;
WIP.completed = completed;
WIP.queued = queueHist;

prodKPIs = evaluateProduction(started,completed,queueHist,operators,machines,releaseRate * 24);

end
