T=struct
z=find(mu<220);
mu=mu(z);
TP=TP(z);
releaseRate=releaseRate(z);

[TPsorted,z]=sort(TP);

for i=1:20
   T(i).z=z(length(z)/20*(i-1)+1:length(z)/20*i);
   T(i).mu=mu(T(i).z);
   T(i).releaseRate=releaseRate(T(i).z);
   end

T(1).z=find(TP<0.1 & mu<200);
for i=2:20
   T(i).z=find(TP>0.1*(i-1) & TP<(0.1*i) & mu<200);
% T(i).z=find(TP>0.1*(i-1) & TP<(0.1*i));
end


figure
for i=1:20
    subplot(4,5,i)
    plot(T(i).releaseRate, T(i).mu,'.')
end