%% Define in and outputs
X=[XA;XB];
Y=[YA;YB];

%% Define variables to be investigated
TP=Y(:,1);
releaseRate=X(:,9);
a=X(:,7);
b=X(:,8);

mu=a.*b;
sigma=sqrt(a).*b;
var=sigma./mu;


figure
scatter(releaseRate,TP,'.')
xlabel('Release Rate')
ylabel('Throughput')

figure
scatter(mu,releaseRate,[],TP,'.')
colormap(jet)
xlim([0 200])
% caxis([20 50])
xlabel('Work Content mean')
ylabel('Release Rate')
c = colorbar;
c.Label.String = 'Throughput';

figure
scatter(releaseRate,TP,[],var,'.')
colormap(jet)
xlabel('Release Rate')
ylabel('Throughput')
c = colorbar;
c.Label.String = 'Variation coefficient';

%% Divide TP into intervals
T=struct
T(1).z=find(TP<0.1 & mu<200);
for i=2:10
   T(i).z=find(TP>0.1*(i-1) & TP<(0.1*i) & mu<200);
% T(i).z=find(TP>0.1*(i-1) & TP<(0.1*i));
end

%% Smaller intervals for TP, to draw the ISO lines
T2=struct
T2(1).z=find(TP>0.09 & TP<0.11);
for i=2:10
   T2(i).z=find(TP>0.09+0.1*(i-1) & TP<0.11+0.1*(i-1)); 
end

%% Divide the WC mean into intervals
Tmu=struct
Tmu(1).ind=find(mu<10);
for i=2:20
   Tmu(i).ind=find(mu>10*(i-1) & mu<(10*i));    
end

%% Divide the standard deviation of the WC into intervals
Tsigma=struct
Tsigma(1).ind=find(sigma<3);
for i=2:20
   Tsigma(i).ind=find(sigma>3*(i-1) & sigma<(3*i));    
end



%% Divide the 'a' into intervals
Ta=struct;
Ta(1).z=find(a<2);
for i=2:20
   Ta(i).z=find(a>2*(i-1) & a<(2*i));    
end

%% Divide 'b' into intervals
Tb=struct;
Tb(1).z=find(b<0.2);
for i=2:20
   Tb(i).z=find(b>0.2*(i-1) & b<(0.2*i));    
end

%% Divide 'var' into intervals
Tvar=struct;
Tvar(1).z=find(var<0.05);
for i=2:20
   Tvar(i).z=find(var>0.05*(i-1) & var<(0.05*i));    
end

Tvar(1).muTP=0;
Tvar(1).stdTP=0;
for i=2:20
    tmp=TP(Tvar(i).z);
    Tvar(i).muTP=mean(tmp);
    Tvar(i).stdTP=std(tmp);
%    Tvar(i).muTP=mean(TP(Tvar(i).z));
%    Tvar(i).stdTP=std(TP(Tvar(i).z));
end

%%plot the mean of the TP with intervals of the variation coefficient
figure
for i=2:20
   subplot(5,4,i-1)
   plot(releaseRate(Tvar(i).z),TP(Tvar(i).z),'.') 
   hold on
   plot([0 12],[Tvar(i).muTP Tvar(i).muTP])
   plot([0 12],[Tvar(i).muTP+1.96*Tvar(i).stdTP Tvar(i).muTP+1.96*Tvar(i).stdTP],'--')
      plot([0 12],[Tvar(i).muTP-1.96*Tvar(i).stdTP Tvar(i).muTP-1.96*Tvar(i).stdTP],'--')
   xlabel('Release Rate')
   ylabel('Throughput')
   tmp=['var in [' , num2str(0.05*(i-1)) , ' - ' , num2str(0.05*(i)) , ']']
   title(tmp)
end

%% Calculate and plot the mean throughput according to the variation coefficient
meanTP=0; variationCoef=0; stdTP=0;
for i=2:20
   meanTP(i-1)=Tvar(i).muTP;
   stdTP(i-1)=Tvar(i).stdTP;
   variationCoef(i-1)=i/20-0.05/2;
end

    fitTPvar = fit(variationCoef',meanTP','poly2')


figure
scatter(variationCoef,meanTP,'filled')
xlabel('Variation Coefficient of the Work Content')
ylabel('Mean Throughput')
ylim([0 1])
axis equal
xlim([0 1])
grid on
xticks(0:0.1:1)

figure
plot(fitTPvar,variationCoef,meanTP)
xlabel('Variation Coefficient of the Work Content')
ylabel('Mean Throughput')
ylim([0 1])
axis equal
xlim([0 1])
grid on
xticks(0:0.1:1)

%% Divide 'a' and 'b' into intervals
Tab=struct;
Tab(1,1).z=find(b<0.2 & a<2);
for i=2:20
    for j=2:20
   Tab(i,j).z=find(b>0.2*(i-1) & b<0.2*i & a>2*(j-1) & a<j*i);    
    end
end
%%
j=2

figure
for i=1:20
subplot(4,5,i)
scatter(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),'.')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

%% Intervals for the TP
figure
for i=1:10
subplot(2,5,i)
scatter(mu(T(i).z),releaseRate(T(i).z),[],TP(T(i).z),'.')
xlim([0,200])
caxis([0 1])
xlabel('mu')
ylabel('release rate')
hello=['TP between ', num2str(0.1*(i-1)), ' and ', num2str(0.1*i)]
title(hello)
end
sgtitle('TP as a function of release rate and mu')
c = colorbar;
c.Label.String = 'Throughput';








figure
hold on
xlim([0 200])
for i=1:10
    plot(mu(T(i).z),releaseRate(T(i).z),'.')
end
xlabel('Mean work content')
ylabel('Production order release rate')
c = colorbar;
c.Label.String = 'Throughput';
title('TP as a function of the release rate and mu')

figure
scatter(releaseRate,TP,[],mu,'.')
caxis([0 200])
xlabel('Release Rate')
ylabel('Throughput')
% colorbar('name','mean of WC')
colormap(jet)
c = colorbar;
c.Label.String = 'Mean Work content';


% figure
% for i=6;
%     releaseratetmp=releaseRate(Tmu(i).ind);
%     TPtmp=TP(Tmu(i).ind);
% scatter(releaseRate(Tmu(i).xtmp),TP(Tmu(i).z),'.')
% xlabel('Release Rate')
% ylabel('Throughput')
% hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
% title(hello)
% end

syms t;
assume(t>0);
assume(t<1)
x1=1;
y1=1;
c1=0.25;
alfa1=10;

x=x1*(1-(1-t^c1)^(1/c1))+alfa1*x1*t;
y=y1*(1-(1-t^c1)^(1/c1));

figure
fplot(x,y)  

figure
hold on
for x1=0.05:round(max(releaseRate))
    
x=x1*(1-(1-t^c1)^(1/c1))+alfa1*x1*t;
y=y1*(1-(1-t^c1)^(1/c1));
fplot(x,y)  
xlim([0 12])

end

%% Calculate fits for TP as a function of Release Rate
% Tmu contains:
% - the indices for the division according to intervals
% - xtmp the indices of the points
% - The function of the fit
% - The function of the derivative of the fit
syms x

% Tmu(1).xtmp=1;
% Tmu(1).df=1;
% Tmu(1).f2=1;
for i=1:20
    ft= fittype('a*(1-exp(b*x))');
   Tmu(i).f=fit(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),ft,'Start',[1 -2]) ;
   Tmu(i).xtmp=linspace(0,12,length(releaseRate(Tmu(i).ind)));
   Tmu(i).xtmp2=linspace(0,12,10*length(releaseRate(Tmu(i).ind)));
   
   Tmu(i).df=differentiate(Tmu(i).f,Tmu(i).xtmp2);
   Tmu(i).f2=Tmu(i).f(Tmu(i).xtmp2);
   Tmu(i).z=min(find(abs(Tmu(i).f2-ones(10*length(releaseRate(Tmu(i).ind)),1)*0.6)<0.02));
  if isempty(Tmu(i).z)
         Tmu(i).z=min(find(abs(Tmu(i).f2-ones(10*length(releaseRate(Tmu(i).ind)),1)*0.6)<0.04));
  end
  if isempty(Tmu(i).z)
         Tmu(i).z=min(find(abs(Tmu(i).f2-ones(10*length(releaseRate(Tmu(i).ind)),1)*0.6)<0.06));
  end
   Tmu(i).dfz=Tmu(i).df(Tmu(i).z);
   
   Tmu(i).sigma=sqrt(a(Tmu(i).ind)).*b(Tmu(i).ind);
   Tmu(i).var=var(Tmu(i).ind);
end

%% Same procedure with intervals for the standard deviation instead of the mean
syms x;
for i=1:20
    ft= fittype('a*(1-exp(b*x))');
   Tsigma(i).f=fit(releaseRate(Tsigma(i).ind),TP(Tsigma(i).ind),ft,'Start',[1 -2]) ;
   Tsigma(i).xtmp=linspace(0,12,length(releaseRate(Tsigma(i).ind)));
   Tsigma(i).xtmp2=linspace(0,12,10*length(releaseRate(Tsigma(i).ind)));
   
   Tsigma(i).df=differentiate(Tsigma(i).f,Tsigma(i).xtmp2);
   Tsigma(i).f2=Tsigma(i).f(Tsigma(i).xtmp2);
   Tsigma(i).z=min(find(abs(Tsigma(i).f2-ones(10*length(releaseRate(Tsigma(i).ind)),1)*0.6)<0.02));
  if isempty(Tsigma(i).z)
         Tsigma(i).z=min(find(abs(Tsigma(i).f2-ones(10*length(releaseRate(Tsigma(i).ind)),1)*0.6)<0.04));
  end
  if isempty(Tsigma(i).z)
         Tsigma(i).z=min(find(abs(Tsigma(i).f2-ones(10*length(releaseRate(Tsigma(i).ind)),1)*0.6)<0.06));
  end
   Tsigma(i).dfz=Tsigma(i).df(Tsigma(i).z);
   
   Tsigma(i).sigma=sqrt(a(Tsigma(i).ind)).*b(Tsigma(i).ind);
   Tsigma(i).var=var(Tsigma(i).ind);
end

%% Same procedure with intervals of the throughput
for i=1:10
%     T(i).f = fit(releaseRate(T(i).z), mu(T(i).z), 'rat01')
    T(i).f = fit(releaseRate(T(i).z), mu(T(i).z),'power2')
    T(i).x=linspace(0,12,1000);
    T(i).xlog=logspace(-2,log10(12),1000)
    T(i).f2=T(i).f(T(i).x);
    T(i).f3=T(i).f(T(i).xlog);
    
end

    figure
   c=lines(2);
    for i=1:10
        subplot(2,5,i)
        plot(T(i).f,releaseRate(T(i).z), mu(T(i).z),'k.')
        xlabel('Release Rate')
        ylabel('Mean Work Content')
        name=['TP between ', num2str(0.1*(i-1)), ' and ', num2str(0.1*i)]
        title(name)
        xlim([-0.2 12.5])
        ylim([-10 220])
        legend off
    end

figure
   c=lines(4);
    for i=1:10
        subplot(5,2,i)
        plot(releaseRate(T(i).z), mu(T(i).z),'.','Color',c(1,:))
        hold on
        
        plot(T(i).x,T(i).f2,'Color',c(2,:),'Linewidth',2)
        xlabel('Release Rate')
        ylabel('Mean WC')
        name=['TP between ', num2str(0.1*(i-1)), ' and ', num2str(0.1*i)]
        title(name)
        xlim([-0.2 12.5])
        ylim([-10 220])
        legend off
    end
    
    
    
     figure
   
    for i=1:10
        subplot(2,5,i)
        plot(T(i).x,T(i).f2,'k.')
        xlabel('Release Rate')
        ylabel('Mean Work Content')
        name=['TP between ', num2str(0.1*(i-1)), ' and ', num2str(0.1*i)]
        title(name)
        xlim([-0.2 12.5])
        ylim([-10 220])
        legend off
    end
    
figure
hold on
for i=1:10
   semilogx(T(i).x,T(i).f2)
end
xlim([-0.2 12.5])
ylim([-10 220])
        
figure
subplot(211)
c=jet(11); l="hello";
semilogx(T(1).xlog,T(1).f3,'Linewidth',1.5,'Color',c(2,:))
l(1)=['TP in [',num2str(0), ' - ', num2str(0.1), ']']
hold on
for i=2:10
   semilogx(T(i).xlog,T(i).f3,'Linewidth',1.5,'Color',c(1+i,:))
   l(i)=['TP in [',num2str((i-1)*0.1), ' - ', num2str(0.1*i), ']']
end
xlim([-0.2 12.5])
ylim([-10 220])
l=legend(l);
l.NumColumns=2;

        
        
%% Plot the fits with the scatter plots
figure
for i=1:20
subplot(4,5,i)
scatter(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),'.')
hold on
plot(Tmu(i).f,'k')
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z))
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
    hello=['mu in [', num2str(10*(i-1)),' - ', num2str(10*i),']']
title(hello)
end
sgtitle('Throughput as a function of the release rate')

%% Plot the fits ONLY
figure
for i=1:20
subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')


%% Plot the fits with their derivatives
figure
for i=1:20
subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
hold on
plot(Tmu(i).xtmp2,Tmu(i).df)
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')


%% Plot the fits with their derivatives
figure
for i=1:20
subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
hold on
plot(Tmu(i).xtmp2,Tmu(i).df)
if ~isempty(Tmu(i).z)
    scatter([Tmu(i).xtmp2(Tmu(i).z),Tmu(i).xtmp2(Tmu(i).z)],[Tmu(i).df(Tmu(i).z),Tmu(i).f2(Tmu(i).z)])
end 
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

% Tz1 contains the derivative of the TP at TP=0.6
Tz(1)=0;
i=1;
for i=1:20
    if ~isempty(Tmu(i).z)
Tz1(i)=Tmu(i).df(Tmu(i).z);
   end
end
   % mus contains the WC
mus=linspace(1,20,length(Tz1));

figure
plot(mus*10,Tz1)
xlabel('Mean work content [h]')
ylabel('Derivative of the TP for the point where TP=60%')
title('Relationship between the derivative of the throughput and the mean WC')



%% Use analogy with Condensator
% Find Tau
for i=1:20
slope=Tmu(i).df(1);

    Tmu(i).tau=Tmu(i).xtmp2(min(find(abs(Tmu(i).f.a-slope*Tmu(i).xtmp2)<0.01)));
if isempty(Tmu(i).tau)
     Tmu(i).tau=Tmu(i).xtmp2(min(find(abs(Tmu(i).f.a-slope*Tmu(i).xtmp2)<0.02))); 
end
if isempty(Tmu(i).tau)
     Tmu(i).tau=Tmu(i).xtmp2(min(find(abs(Tmu(i).f.a-slope*Tmu(i).xtmp2)<0.03))); 
end
end


figure
for i=1:20
slope=Tmu(i).df(1);
    subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
hold on
plot(Tmu(i).xtmp2,Tmu(i).f.a*ones(length(Tmu(i).xtmp2),1))
plot(Tmu(i).xtmp2,slope*Tmu(i).xtmp2)

scatter([Tmu(i).tau Tmu(i).tau],[Tmu(i).f(Tmu(i).tau) Tmu(i).f.a])

xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

tau=0, as=0; sigmas=0;
for i=1:20
   tau(i)=-1/(Tmu(i).f.b);
   as(i)=Tmu(i).f.a
   sigmas(i)=sqrt(a(Tmu(i).ind)).*b(Tmu(i).ind);
end


figure
plot(mus*10,tau)
xlabel('Mean Work Content [h]')
ylabel('Tau, Release rate to reach 63% of the maximum TP')
hold on
plot(mus*10,as)
legend('Tau','Max TP')


%% Plot the fits with the scatter plots
figure
for i=1:20
subplot(4,5,i)
scatter(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),[],sqrt(a(Tmu(i).ind)).*b(Tmu(i).ind),'.')
% caxis([0 70])
colormap(jet)
colorbar
hold on
plot(Tmu(i).f,'k')
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z))
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')




%% test variables
releaseratetest=releaseRate(T(i).z);
mutest=mu(T(i).z);
