fn='Cambria Math';
fs = 11
ec  = 'k' ; % color of edges
% You can produce a coloured plot or a black and white one
% (printer-friendly). Furthermore, you can use matlab colourmaps or
% repeat 5 'easy-to-distinguish' colours (see http://colorbrewer2.org/).
% Option 1a - coloured using colorbrewer: uncomment the following line:
col = [[228,26,28];[55,126,184];[77,175,74];[152,78,163];[255,127,0]]/256;

%% Example for MCM
%% Generate points with the MCM
X10=rand(10,2);
X100=rand(100,2);
X1000=rand(1000,2);

% Generate points with the LHS
LHS10=lhcube(10,2);
LHS100=lhcube(100,2);
LHS1000=lhcube(1000,2);

%% Scatter plot of the X10 points
figure
scatter(X10(:,1),X10(:,2),'filled')
xlabel('X1');
ylabel('X2')
grid on
xticks(0:0.1:1)
yticks(0:0.1:1)
axis equal
xlim ([0 1])
ylim([0 1])

%% Scatter plot for X100
figure
scatterhist(X100(:,1),X100(:,2),'Marker','.','NBins',10)
xlabel('X1');
ylabel('X2')
grid on
xticks(0:0.2:1)
yticks(0:0.2:1)

axis equal
xlim ([0 1])
ylim([0 1])

%% Scatter plot for X1000
figure
scatter(X1000(:,1),X1000(:,2),'filled')
xlabel('X1');
ylabel('X2')
grid on
xticks(0:0.1:1)
set(gca, 'XTickLabel', [])

yticks(0:0.1:1)
set(gca, 'YTickLabel', [])

axis equal
xlim ([0 1])
ylim([0 1])


figure
scatterhist(X1000(:,1),X1000(:,2),'Marker','.')
xlabel('X1');
ylabel('X2')
grid on
xticks(0:0.1:1)
yticks(0:0.1:1)
axis equal
xlim ([0 1])
ylim([0 1])



%% Scatter plot for LHS100
figure
scatterhist(LHS100(:,1),LHS100(:,2),'Marker','.','NBins',10)
xlabel('X1');
ylabel('X2')
grid on
xticks(0:0.2:1)
yticks(0:0.2:1)

axis equal
xlim ([0 1])
ylim([0 1])

%% Example with sensitivity indices
S=[0.7 0.6 0.04 0.02 0.02];

figure
boxplot1(S)
title('This is a test')
xlabel('This is still a test')
% modify figure size

%% Use this code section after generating the plot to obtain the desired size
xpos=5;ypos=5;
h = gcf; % Current figure handle
% set(h,'Resize','off');
% set(h,'PaperPositionMode','manual');
set(h,'Units','centimeters');
h.Units='centimeters';

set(h,'PaperPosition',[0 0 9 6]);
set(h,'PaperSize',[13 20]); % IEEE columnwidth = 9cm
set(h,'Position',[xpos ypos 12.5 9]);
% xpos, ypos must be set
% txlabel = text(xpos,ypos,'$$[\mathrm{min}]$$','Interpreter','latex','FontSize',9);
% % Dump colored encapsulated PostScript
% print('-depsc2','-loose', 'signals');
set(gca,'FontSize',11,'FontName','Cambria Math')
set(findall(gcf,'-property','FontSize'),'FontSize',11,'FontName','Cambria Math','FontWeight','Normal')

% ylabel(Y_Label,'FontSize',11,'FontName','Cambria Math')

%% Example with Monte Carlo Method




%% modify figure size
xpos=5;ypos=5
h = gcf; % Current figure handle
set(h,'Resize','off');
set(h,'PaperPositionMode','manual');
set(h,'PaperPosition',[0 0 9 6]);
set(h,'PaperUnits','centimeters');
set(h,'PaperSize',[9 6]); % IEEE columnwidth = 9cm
set(h,'Position',[0 0 9 6]);
% xpos, ypos must be set
txlabel = text(xpos,ypos,'$$[\mathrm{min}]$$','Interpreter','latex','FontSize',9);
% Dump colored encapsulated PostScript
print('-depsc2','-loose', 'signals');


%% save figure as scalable vector graphic
saveas(figure (2), 'test2.svg')


%% Summary plot for factor prioritization
figure
%VBSA
subplot(4,1,1)
 boxplot1(SiTP,X_Labels,'VBSA - Main effects')
 set(gca, 'XTickLabel', [])
    pos = get(gca, 'Position');
    pos(4) = pos(4)*1.2;
    set(gca, 'Position', pos)
    
 %RSA
 subplot(4,1,2)
 boxplot1(mvd,X_Labels,' RSA - MVD')
set(gca, 'XTickLabel', [])
    pos = get(gca, 'Position');
    pos(4) = pos(4)*1.2;
    set(gca, 'Position', pos)

%PAWN
subplot(4,1,3)
boxplot1(Spawn_m,X_Labels,'PAWN - Sensitivity Indices')
xtickangle(45)

pos = get(gca, 'Position');
    pos(4) = pos(4)*1.2;
    set(gca, 'Position', pos)

    
    
    %% Plot RSA points
   
    indices=[];
k=0;
for i=1:length(X)
    if idxb(i,1)==1
        k=k+1;
        indices(k,1)=i;
    end
end

Xnew=X(indices,:);
Ynew=Y(indices,:);
    
    figure
    for i=1:9
       subplot(3,3,i)
       plot(X(:,i),Y(:,1),'b.',)
       hold on
       for j=1
       plot(Xnew(:,i),Ynew(:,1),'r.')
       end
    
       