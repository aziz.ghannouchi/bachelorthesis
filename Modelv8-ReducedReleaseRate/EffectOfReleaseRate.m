%% Define in and outputs
X=[XA;XB];
Y=[YA;YB];

%% Define variables to be investigated
TP=Y(:,1);
releaseRate=X(:,9);
a=X(:,7);
b=X(:,8);

mu=a.*b;
sigma=sqrt(a).*b;
var=sigma./mu;


figure
scatter(releaseRate,TP,'.')
xlabel('Release Rate')
ylabel('Throughput')

figure
scatter(mu,releaseRate,[],TP,'.')
colormap(jet)
xlim([0 200])
% caxis([20 50])
xlabel('Work Content mean')
ylabel('Release Rate')
c = colorbar;
c.Label.String = 'Throughput';

figure
scatter(releaseRate,TP,[],var,'.')
colormap(jet)
xlabel('Release Rate')
ylabel('Throughput')
c = colorbar;
c.Label.String = 'Variation coefficient';

%% Divide TP into intervals
T=struct
T(1).z=find(TP<0.1);
for i=2:10
   T(i).z=find(TP>0.1*(i-1) & TP<(0.1*i)); 
end

%% Divide the WC mean into intervals
Tmu=struct
Tmu(1).ind=find(mu<10);
for i=2:20
   Tmu(i).ind=find(mu>10*(i-1) & mu<(10*i));    
end

%% Divide the 'a' into intervals
Ta=struct;
Ta(1).z=find(a<2);
for i=2:20
   Ta(i).z=find(a>2*(i-1) & a<(2*i));    
end

%% Divide 'b' into intervals
Tb=struct;
Tb(1).z=find(b<0.2);
for i=2:20
   Tb(i).z=find(b>0.2*(i-1) & b<(0.2*i));    
end

%% Divide 'var' into intervals
Tvar=struct;
Tvar(1).z=find(var<0.05);
for i=2:20
   Tvar(i).z=find(var>0.05*(i-1) & b<(0.05*i));    
end

%% Divide 'a' and 'b' into intervals
Tab=struct;
Tab(1,1).z=find(b<0.2 & a<2);
for i=2:20
    for j=2:20
   Tab(i,j).z=find(b>0.2*(i-1) & b<0.2*i & a>2*(j-1) & a<j*i);    
    end
end
%%
j=2

figure
for i=1:20
subplot(4,5,i)
scatter(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),'.')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

figure
for i=1:10
subplot(2,5,i)
scatter(mu(T(i).z),releaseRate(T(i).z),[],TP(T(i).z),'.')
xlim([0,200])
caxis([0 1])
xlabel('mu')
ylabel('release rate')
hello=['TP between ', num2str(0.1*(i-1)), ' and ', num2str(0.1*i)]
title(hello)
end
sgtitle('TP as a function of release rate and mu')
c = colorbar;
c.Label.String = 'Throughput';

figure
hold on
xlim([0 200])
for i=1:10
    plot(mu(T(i).z),releaseRate(T(i).z),'.')
end
xlabel('Mean work content')
ylabel('Production order release rate')
c = colorbar;
c.Label.String = 'Throughput';
title('TP as a function of the release rate and mu')

figure
scatter(releaseRate,TP,[],mu,'.')
caxis([0 200])
xlabel('Release Rate')
ylabel('Throughput')
% colorbar('name','mean of WC')
colormap(jet)
c = colorbar;
c.Label.String = 'Mean Work content';


% figure
% for i=6;
%     releaseratetmp=releaseRate(Tmu(i).ind);
%     TPtmp=TP(Tmu(i).ind);
% scatter(releaseRate(Tmu(i).xtmp),TP(Tmu(i).z),'.')
% xlabel('Release Rate')
% ylabel('Throughput')
% hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
% title(hello)
% end

syms t;
assume(t>0);
assume(t<1)
x1=1;
y1=1;
c1=0.25;
alfa1=10;

x=x1*(1-(1-t^c1)^(1/c1))+alfa1*x1*t;
y=y1*(1-(1-t^c1)^(1/c1));

figure
fplot(x,y)  

figure
hold on
for x1=0.5:round(max(releaseRate))
    
x=x1*(1-(1-t^c1)^(1/c1))+alfa1*x1*t;
y=y1*(1-(1-t^c1)^(1/c1));
fplot(x,y)  
xlim([0 12])

end

%% Calculate fits for TP as a function of Release Rate
% Tmu contains:
% - the indices for the division according to intervals
% - xtmp the indices of the points
% - The function of the fit
% - The function of the derivative of the fit
syms x

% Tmu(1).xtmp=1;
% Tmu(1).df=1;
% Tmu(1).f2=1;
for i=1:20
   Tmu(i).f=fit(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),'exp2') ;
   Tmu(i).xtmp=linspace(0,12,length(releaseRate(Tmu(i).ind)));
   Tmu(i).df=differentiate(Tmu(i).f,Tmu(i).xtmp);
   Tmu(i).f2=Tmu(i).f(Tmu(i).xtmp);
   Tmu(i).z=min(find(abs(Tmu(i).f2-ones(length(releaseRate(Tmu(i).ind)),1)*0.6)<0.02));
  if isempty(Tmu(i).z)
         Tmu(i).z=min(find(abs(Tmu(i).f2-ones(length(releaseRate(Tmu(i).ind)),1)*0.6)<0.04));
  end
  if isempty(Tmu(i).z)
         Tmu(i).z=min(find(abs(Tmu(i).f2-ones(length(releaseRate(Tmu(i).ind)),1)*0.6)<0.06));
  end
   Tmu(i).dfz=Tmu(i).df(Tmu(i).z);
end

%% Plot the fits with the scatter plots
figure
for i=1:20
subplot(4,5,i)
scatter(releaseRate(Tmu(i).ind),TP(Tmu(i).ind),'.')
hold on
plot(Tmu(i).f,'k')
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z))
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

%% Plot the fits ONLY
figure
for i=1:20
subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')


%% Plot the fits with their derivatives
figure
for i=1:20
subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
hold on
plot(Tmu(i).xtmp,Tmu(i).df)
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')


%% Plot the fits with their derivatives
figure
for i=1:20
subplot(4,5,i)
% plot(Tmu(i).f,xx,'k')
xlim([0 12])
% plot(Tmu(i).f,'k',releaseRate(Tmu(i).z),TP(Tmu(i).z),'w')
plot(Tmu(i).f)
hold on
plot(Tmu(i).xtmp,Tmu(i).df)
if ~isempty(Tmu(i).z)
    scatter([Tmu(i).xtmp(Tmu(i).z),Tmu(i).xtmp(Tmu(i).z)],[Tmu(i).df(Tmu(i).z),Tmu(i).f2(Tmu(i).z)])
end 
xlim([0 12])
ylim([0 1.1])
hLeg = legend('example')
set(hLeg,'visible','off')
xlabel('Release Rate')
ylabel('Throughput')
hello=['mu between ', num2str(10*(i-1)),' and ', num2str(10*i)]
title(hello)
end
sgtitle('Throughput as a function of the release rate')

% Tz contains the derivative of the TP at TP=0.6
for i=1:20
   if ~isempty(Tmu(i).z)
Tz1(i)=Tmu(i).df(Tmu(i).z)
   end
end