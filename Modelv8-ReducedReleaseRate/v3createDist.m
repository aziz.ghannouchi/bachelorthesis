function dist = createDist(param1,param2,param3)
%figure
%skewed normal distribution
% x=-5:0.1:5;
% normDist = @(x) (1/sqrt((2*pi))*exp(-x.^2/2));
% skewNormDist = @(x,alpha) 2*normDist(x).*normcdf(alpha*x);
%plot(x, skewNormDist(x, 6));

%f distribution
% x=0:0.1:5;
% plot(x, fpdf(x, 10,10));

%weibull distribution
%dist = makedist('Weibull',50,2.3);

%burr distribution
%dist = makedist('Burr',param1,param2,param3);

%gamma
%dist = makedist('Gamma',param1,param2);

%lognormal
%dist = makedist('Lognormal',param1,param2);

%uniform distribution
%dist = makedist('Uniform');

%normal distribution (gaussian)
%dist = makedist('Normal',param1,param2);

%weibull distribution
dist = makedist('Gamma',param1,param2);
end



