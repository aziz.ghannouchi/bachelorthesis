function schedule = createSchedule(jobCount,matCount,dist)

schedule = ones(jobCount, 4);

schedule(:,1) = ceil(random(dist,jobCount,1));
%randomly select a material
schedule(:,2) = randi(matCount,jobCount,1);

schedule(:,3) = schedule(:,1);
schedule(:,4) = zeros();

end


